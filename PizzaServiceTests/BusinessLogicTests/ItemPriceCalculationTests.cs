﻿using Moq;
using PizzaService;
using PizzaService.BusinessLogic;
using PizzaService.PizzaServiceLocator;
using PizzaServiceDB.DbBase;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Xunit;
using PC = PizzaService.PizzaServiceConstants;
namespace PizzaServiceTests.BusinessLogicTests
{
    public class ItemsInlineConfigurations : IEnumerable<object[]>
    {
        public static IEnumerable<object[]> GetItemsFromInlineConfigurations()
        {
            yield return new object[]
              {
                 "Single", 1m, 2,
                new Items { Id = 6, ItemName="Salami mit Schinken,Salami,Peproni und gemischtem Salat", ItemId = 106, listSubItems = null,
                                             PriceSingle = 5m, PriceJumbo = 7.5m, PriceFamily = 9.6m, PriceParty = 13m
                          },
                10.00m
              };
            yield return new object[]
             {
                "Jumbo", 1.5m, 3,
                 new Items{ Id = 10, ItemName="Tonno,Thunfisch,Zweibel", ItemId = 110, listSubItems = null,
                                         PriceSingle = 3m, PriceJumbo = 5m, PriceFamily = 7.6m, PriceParty = 10.6m
                          },
                 15.00m
             };
        }
        public IEnumerator<object[]> GetEnumerator() => GetItemsFromInlineConfigurations().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class ItemPriceCalculationTests
    {
        private DataGridView CustomerItemDataGridView()
        {
            var dataGridView1 = new DataGridView { ColumnCount = 5 };
            dataGridView1.Columns[0].Name = PC.NUMMER;
            dataGridView1.Columns[1].Name = PC.ANZAHL;
            dataGridView1.Columns[2].Name = PC.NAME;
            dataGridView1.Columns[3].Name = PC.GRÖSSE;
            dataGridView1.Columns[4].Name = PC.PREIS;
            dataGridView1.AllowUserToAddRows = true;
            return dataGridView1;
        }

        [Fact]
        public void SetCurrentCustomer_InvalidCustomer_NullReferenceException()
        {
            //arrange
            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);
            //act
            var exception = Record.Exception(() => sut.SetCurrentCustomer(null));
            //assert
            Assert.NotNull(exception);
            Assert.IsType<NullReferenceException>(exception);
        }

        [Fact]
        public void SetCurrentCustomer_ValidCustomer_Sucess()
        {
            //arrange
            var cus = new Customer()
            {
                Id = 2,
                FirstName = "pqr",
                LastName = "DDD",
                Address = "XYZ Str.",
                Phone = "1234",
                City = "abc",
                Postalcode = 855579
            };

            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);

            //act
            sut.SetCurrentCustomer(cus);

            //assert
            Assert.Equal("abc", cus.City);
            Assert.Equal("pqr", cus.FirstName);
            Assert.Equal("DDD", cus.LastName);
            Assert.Equal("XYZ Str.", cus.Address);
            Assert.Equal("1234", cus.Phone);
            Assert.Equal(855579, cus.Postalcode);
            Assert.Equal(2, cus.Id);
        }

        [Fact]
        public void SetCurrentCustomer_ValidCustomer_Failure()
        {
            //arrange
            var cus = new Customer()
            {
                Id = 2,
                FirstName = "pqr",
                LastName = "DDD",
                Address = "XYZ Str.",
                Phone = "1234",
                City = "abc",
                Postalcode = 855579
            };

            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);
            //act
            sut.SetCurrentCustomer(cus);
            //assert
            Assert.NotEqual(" ", cus.City);
            Assert.NotEqual(" ", cus.FirstName);
            Assert.NotEqual("DDD ", cus.LastName);
            Assert.NotEqual("XYZ Str", cus.Address);
            Assert.NotEqual(" ", cus.Phone);
            Assert.NotEqual(0, cus.Postalcode);
            Assert.NotEqual(-6, cus.Id);
        }

        [Fact]
        public async Task CalculateItemPriceAsync_InValidCustomerOrderGridRow_ThrowsArgumentNullException()
        {
            //arrange
            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);
            //act
            // ReSharper disable once PossibleNullReferenceException
            var exception = await Record.ExceptionAsync(() => sut.CalculateItemPriceAsync(null));

            //assert
            Assert.IsType<ArgumentNullException>(exception);
            Assert.Contains("CurrentRow is null", exception.Message);
        }

        [Fact]
        public async Task CalculateItemPriceAsync_NotFoundItem_ThrowsArgumentException()
        {
            //arrange
            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();

            mockItems.Setup(moq => moq.SearchForAsync(It.IsAny<Expression<Func<Items, bool>>>(),
                                                      It.IsAny<string>()))
                                                      .Callback<Expression<Func<Items, bool>>, string>((_, __) => { })
                                                      .ReturnsAsync(new List<Items>());

            var dataGridView1 = CustomerItemDataGridView();

            dataGridView1.Rows.Add(104, 1, "www", "Single");

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);
            //act
            // ReSharper disable once PossibleNullReferenceException
            var exception = await Record.ExceptionAsync(() => sut.CalculateItemPriceAsync(dataGridView1.Rows[0]));
            //assert
            Assert.IsType<ArgumentException>(exception);
            Assert.Contains($"Type {typeof(Items)} does not exit.", exception.Message);
        }

        [Theory]
        [MemberData(nameof(ItemsInlineConfigurations.GetItemsFromInlineConfigurations), MemberType = typeof(ItemsInlineConfigurations))]
        public async Task CalculateItemPriceAsync_FoundItem_ValidPrice(string size, decimal configPrice, int numOfItems, Items item, decimal expected)
        {
            //arrange
            var mockItems = new Mock<IPizService<Items>>();
            var mockPLogger = new Mock<IPizzaServiceLogger>();
            var mockConfigOption = new Mock<IConfiguration>();
            var cus = new Customer()
            {
                Id = 2,
                FirstName = "pqr",
                LastName = "DDD",
                Address = "XYZ Str.",
                Phone = "1234",
                City = "abc",
                Postalcode = 855579
            };

            mockItems.Setup(moq => moq.SearchForAsync(It.IsAny<Expression<Func<Items, bool>>>(),
                                                      It.IsAny<string>()))
                                                      .Callback<Expression<Func<Items, bool>>, string>((_, __) => { })
                                                      .ReturnsAsync(new List<Items> { item });

            mockConfigOption.Setup(mock => mock.GetExtraConfigSetting(It.IsAny<string>())).Returns(configPrice);

            var dataGridView1 = CustomerItemDataGridView();
            dataGridView1.Rows.Add(item.ItemId, numOfItems, item.ItemName, size, item.PriceSingle);

            var sut = new ItemPriceCalculation(mockItems.Object, mockPLogger.Object, mockConfigOption.Object);
            sut.SetCurrentCustomer(cus);

            //act
            var actual = await sut.CalculateItemPriceAsync(dataGridView1.Rows[0]);
            //assert
            Assert.Equal(expected, actual);
        }
    }
}