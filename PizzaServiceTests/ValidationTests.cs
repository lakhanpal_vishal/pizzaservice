﻿using Xunit;
namespace PizzaService.Tests
{
    public class ValidationTests
    {
        [Theory]
        [InlineData("")]
        public void ValidateNumber_EmptyData_False(string strEmptyData)
        {
            //arrange
            var sut = new Validation();
            //act & assert
            Assert.False(sut.ValidateNumber(strEmptyData));
        }

        [Theory]
        [InlineData("bb12wer")]
        [InlineData("frr12wner")]
        [InlineData("ff1nn2wer ÖÄÄ")]
        public void ValidateNumber_InvalidData_False(string strInvalidData)
        {
            var sut = new Validation();
            Assert.False(sut.ValidateNumber(strInvalidData));
        }

        [Theory]
        [InlineData("000123456")]
        [InlineData("123433456")]
        [InlineData("12343333456")]
        public void ValidateNumber_ValidData_True(string strValidData)
        {
            var sut = new Validation();
            Assert.True(sut.ValidateNumber(strValidData));
        }

        [Theory]
        [InlineData(" ^ ^°  °°xcc  xc1234...")]
        [InlineData("frr1/(  )  ))2wner")]
        [InlineData("ff 1nn2we r????ÖÄÄ")]
        public void ValidateText_InvalidData_False(string strInvalidData)
        {
            var sut = new Validation();
            Assert.False(sut.ValidateText(strInvalidData));
        }

        [Theory]
        [InlineData("xccx  c  / \\ --1 ää234")]
        [InlineData("  xccx  c  / \\ --1 --- _ _ ää234")]
        [InlineData("1234  3333456")]
        public void ValidateText_ValidData_True(string strValidData)
        {
            var sut = new Validation();
            Assert.True(sut.ValidateText(strValidData));
        }
    }
}


