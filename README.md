PizzaService, a Winforms Desktop Application.

This README documents the necessary steps to get your application up and running.

What is this repository for?

    Quick summary:
    This software helps the pizza shopowner to achieve following tasks:
         1. Handles the personal data of the new and existing customers.
         2. Handles the customer orders and modify those orders in an efficent manner.
         3. Finally helps to generate the customer bill for his order.
    
How do I get set up?

     This application is being developed exclusively under windows 7/10 and VisualStudio 15/17 and .NetFramework 4.5/4.7.
     
     Configuration and Dependencies for main application(Nuget packages):
          1. log4net, version 2.08
          2. Microsoft.ReportingServices.ReportViewerControl.Winforms, version 150.900.148
          3. Microsoft.SqlServer.Types,version 14.0.1016.290
          4. SimpleInjector, version 4.4.3
          5. EntityFramework, version 6.2 or higher
          6. System.Data.SQLite, version 1.0.109.2 or higher
          7. System.Data.SQLite.EF6, version 1.0.109.2 or higher
          8. System.Data.SQLite.Linq, version 1.0.109.2 or higher
          9. System.Data.SQLite.Core, version 1.0.109.2 or higher
      
      Configuration and Dependencies for unittesting the application(Nuget packages):
          1. Castle.Core, version 4.3.1
          2. Moq, version 4.9.0 or higher
          3. System.Runtime.CompilerServices.Unsafe, version 4.5.1 or higher
          4. System.Threading.Tasks.Extensions, version 4.5.1 or higher
          5. System.ValueTuple,  version 4.5.1 or higher
          6. xunit and its related dependencies, version 2.4.0 or higher

Final Note:

          1. Once the project is opened successfully in VS 2017, right click on the solution and click on "Manage Nuget Packages for the 
             solution" will automatically restore the above mentioned packages with a working internet solution.             
          2. Since VS 2017 does not offer anymore inbuilt crytal reports,we have to add manually RDLC and Reportviewer to our project.
             Kindly search "Enable RDLC Reporting in Visual Studio 2017" in internet and follow the steps there to complete this task.
     
