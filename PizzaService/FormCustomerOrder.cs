﻿using PizzaService.BusinessLogic;
using PizzaService.PizzaServiceLocator;
using PizzaServiceDB.DbBase;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using PC = PizzaService.PizzaServiceConstants;

namespace PizzaService
{
    public partial class FormCustomerOrder : Form
    {
        private static Customer _currentCustomer;
        private readonly List<Tuple<int, IEnumerable<string>, List<SubItems>>> _lstSubItemsTuples;
        private bool _bdataGridViewCustomerOrderHasFocus = true;
        private readonly IItemPriceCalculation _itPriceCalculation;
        private readonly IPizService<Items> _serItems;
        private readonly IPizService<ResultNextFreeRowId> _unOfWork;
        private readonly IPizService<CustomerBills> _serCustomerBills;
        private readonly IPizzaServiceLogger _serLogger;
        /// <summary>
        ///self explanatory
        /// </summary>
        /// <param name="itPriceCalculation"></param>
        /// <param name="serItems"></param>
        /// <param name="unOfWork"></param>
        /// <param name="serCustomerBills"></param>
        /// <param name="serLogger"></param>
        public FormCustomerOrder(IItemPriceCalculation itPriceCalculation,
            IPizService<Items> serItems, IPizService<ResultNextFreeRowId> unOfWork,
            IPizService<CustomerBills> serCustomerBills, IPizzaServiceLogger serLogger
            )
        {
            _itPriceCalculation = itPriceCalculation;
            _serItems = serItems;
            _unOfWork = unOfWork;
            _serCustomerBills = serCustomerBills;
            _serLogger = serLogger;

            _lstSubItemsTuples = new List<Tuple<int, IEnumerable<string>, List<SubItems>>>();
            InitializeComponent();
            PrepareDataGridView();
            //Assign the event handler
            dataGridViewCustomerOrder.EditingControlShowing += dataGridViewCustomerOrder_EditingControlShowing;
            dataGridViewCustomerOrder.CellDoubleClick += dataGridViewCustomerOrder_CellDoubleClick;
        }

        /// <summary>
        /// Combo-box is displayed for the item with subitem category = 'Pizza'.
        /// Only Pizza items have different sizes like Single, Jumbo etc..
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCustomerOrder_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control is ComboBox combo)
            {
                combo.DropDown -= combo_DropDown;
                combo.DropDown += combo_DropDown;
            }
        }

        /// <summary>
        /// Display colors in the Combo-box when it is expanded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void combo_DropDown(object sender, EventArgs e)
        {
            var cb = (ComboBox)sender;
            //Return if the combo box does not exit or current row is invalid
            if (cb == null || dataGridViewCustomerOrder.CurrentRow == null)
                return;
            if (dataGridViewCustomerOrder.CurrentRow.Index % 2 == 0)
            {
                dataGridViewCustomerOrder.CurrentCell.Style.BackColor = Color.White;
                cb.BackColor = Color.White;
            }
            else
            {
                dataGridViewCustomerOrder.CurrentCell.Style.BackColor = Color.PaleGoldenrod;
                cb.BackColor = Color.PaleGoldenrod;
            }
        }

        /// <summary>
        /// Assign the current customer data to the Form controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCustomerOrder_Load(object sender, EventArgs e)
        {
            lblName.Text = _currentCustomer.FirstName + " " + _currentCustomer.LastName;
            lblOrt.Text = _currentCustomer.City;
            lblStrasse.Text = _currentCustomer.Address;
            lblPlz.Text = _currentCustomer.Postalcode.ToString();

            lblTelefon.Text = !_currentCustomer.FirstName.Contains(PC.ABHOLER)
            ?
            _currentCustomer.Id + "/" + _currentCustomer.Phone
            :
            _currentCustomer.Id.ToString();
        }

        /// <summary>
        /// Prints the Customer Bill.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnPrint_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                _serLogger.Log("Start printing of the Bill now.");
                var listCustomerBills = PrepareCustomerBill();
                if (listCustomerBills.Count == 0)
                    return;
                using (var frmCustomerOrderPrint = new FormCustomerOrderPrint(_serLogger))
                {
                    await Task.Run(() => frmCustomerOrderPrint.PrepareToPrint(listCustomerBills, _currentCustomer))
                        .ContinueWith
                        (
                               _ => frmCustomerOrderPrint.Print(),
                              TaskContinuationOptions.OnlyOnRanToCompletion
                        )
                        .ContinueWith
                        (
                             async _ =>
                             {
                                 await SaveCustomerOrderIntoDb(listCustomerBills).ConfigureAwait(false);
                                 if (InvokeRequired)
                                 {
                                     Invoke(new Action(Close));
                                 }
                                 _serLogger.Log("Printing of the Bill done now.");
                             }, TaskContinuationOptions.OnlyOnRanToCompletion
                         ).ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                _serLogger.Log($" Print failed for the Customer Bill: {ex.Message} with InnerException: {ex.InnerException}");
                MessageBox.Show("Fehler aufgetreten!!!");
            }
        }

        /// <summary>
        /// Saves the current customer for the Business logic
        /// </summary>
        public void SaveCustomerData(Customer customer)
        {
            _currentCustomer = customer;
            _itPriceCalculation.SetCurrentCustomer(customer);
        }

        /// <summary>
        /// Update the Price of the current Item and the total Price depending upon
        /// the modifications made by the user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void dataGridViewCustomerOrder_CellValueChangedAsync(object sender, DataGridViewCellEventArgs e)
        {
            var currentcell = dataGridViewCustomerOrder[e.ColumnIndex, e.RowIndex];

            if (dataGridViewCustomerOrder.CurrentRow == null || !dataGridViewCustomerOrder.IsCurrentCellDirty || string.IsNullOrEmpty(currentcell.EditedFormattedValue?.ToString()))
                return;
            _serLogger.Log($"Current Cell Value {currentcell.Value}  changed for the column {currentcell.OwningColumn.Name}");

            if (currentcell.OwningColumn.Name == PC.ANZAHL)
            {
                if (dataGridViewCustomerOrder.CurrentRow.Cells[PC.PREIS].Value == null)
                {
                    _serLogger.Log("Cell with Preis has been null.");
                    //  todo throw new ArgumentException;
                }
                var bIntString = currentcell.EditedFormattedValue.ToString().All(char.IsDigit);
                if (!bIntString)
                {
                    _serLogger.Log("Invalid Text entered by the user, some non digit string has been added.");
                    //   throw;
                }
            }
            else if (currentcell.OwningColumn.Name == PC.GRÖSSE && e.RowIndex >= 0) //check if combobox column
            {
                if (dataGridViewCustomerOrder.CurrentRow.Cells[PC.NUMMER].Value == null)
                {
                    _serLogger.Log("Cell with Nummer has been null.");
                    return;
                }
                string[] arrItemSize = { PC.SINGLE, PC.JUMBO, PC.FAMILY, PC.PARTY };
                if (!arrItemSize.Contains(currentcell.EditedFormattedValue))
                {
                    _serLogger.Log($"Invalid Text entered by the user {currentcell.EditedFormattedValue}");
                    return;
                }
            }
            else if (currentcell.OwningColumn.Name == PC.PREIS)
            {
                dataGridViewCustomerOrder.CurrentRow.Cells[PC.PREIS].Value = Utilites.ConvertStringToDecimal(dataGridViewCustomerOrder.Rows[e.RowIndex].Cells[PC.PREIS].Value.ToString());
            }

            if (currentcell.OwningColumn.Name == PC.ANZAHL || currentcell.OwningColumn.Name == PC.GRÖSSE)
            {
                dataGridViewCustomerOrder.CurrentRow.Cells[PC.PREIS].Value
                    = await _itPriceCalculation.
                    CalculateItemPriceAsync(dataGridViewCustomerOrder.CurrentRow).ConfigureAwait(false);
            }

            //Update the Final Price
            CalculateTotalOrderPrice();
        }

        /// <summary>
        /// This will allow to change the contents(subitems) of the current Row(Item)
        /// in the datagrid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCustomerOrder_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var currentCell = dataGridViewCustomerOrder.CurrentCell;

                if (string.IsNullOrEmpty(currentCell?.EditedFormattedValue?.ToString()))
                {
                    _serLogger.Log("currentCell value has been null.");
                    return;
                }
                //we are concerned only about the double click on the cell
                // which has owning column name = PC.NAME
                if (currentCell.OwningColumn.Name == PC.NAME)
                {
                    var frmCustomerOrderModification = new FormCustomerOrderModification(_itPriceCalculation, _serLogger);
                    Action<DataGridViewRow, List<Tuple<int, IEnumerable<string>, List<SubItems>>>> customer = frmCustomerOrderModification.ModifyCurrentRowContents;
                    customer(dataGridViewCustomerOrder.CurrentRow, _lstSubItemsTuples);
                    frmCustomerOrderModification.StartPosition = FormStartPosition.CenterScreen;
                    var dlgresult = frmCustomerOrderModification.ShowDialog();
                    if (dlgresult == DialogResult.OK)
                    {
                        dataGridViewCustomerOrder.RefreshEdit();
                        dataGridViewCustomerOrder.CurrentCell = dataGridViewCustomerOrder[0, dataGridViewCustomerOrder.Rows.Count - 1];
                        var cellVal = (dataGridViewCustomerOrder.CurrentCell.Value ?? "Unknown").ToString();
                        _serLogger.Log(cellVal);
                    }
                    else
                    {
                        _serLogger.Log("User had pressed the cancel button");
                    }
                }
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Cell Double Click failed: {ex.Message} with InnerException: {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        ////self explanatory
        /// </summary>
        private void PrepareDataGridView()
        {
            dataGridViewCustomerOrder.ColumnCount = 5;
            dataGridViewCustomerOrder.EditMode = DataGridViewEditMode.EditOnEnter;
            dataGridViewCustomerOrder.ColumnHeadersDefaultCellStyle.Font = new Font("Tahoma", 9, FontStyle.Bold, GraphicsUnit.Point);
            dataGridViewCustomerOrder.ColumnHeadersDefaultCellStyle.ForeColor = Color.DarkSlateGray;
            dataGridViewCustomerOrder.EnableHeadersVisualStyles = false;
            dataGridViewCustomerOrder.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCustomerOrder.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCustomerOrder.DefaultCellStyle.Font = new Font("Arial", 10, FontStyle.Bold, GraphicsUnit.Point);
            dataGridViewCustomerOrder.DefaultCellStyle.BackColor = Color.Empty;
            dataGridViewCustomerOrder.AlternatingRowsDefaultCellStyle.BackColor = Color.PaleGoldenrod;
            dataGridViewCustomerOrder.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCustomerOrder.GridColor = SystemColors.ControlDarkDark;
            dataGridViewCustomerOrder.AllowUserToDeleteRows = false;
            dataGridViewCustomerOrder.SelectionMode = DataGridViewSelectionMode.CellSelect;
            dataGridViewCustomerOrder.RowHeadersVisible = false;
            dataGridViewCustomerOrder.BorderStyle = BorderStyle.None;
            dataGridViewCustomerOrder.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            dataGridViewCustomerOrder.Columns[0].Name = PC.NUMMER;
            var gridViewColumn = dataGridViewCustomerOrder.Columns[PC.NUMMER];
            if (gridViewColumn != null)
                gridViewColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCustomerOrder.Columns[0].DefaultCellStyle.ForeColor = Color.Maroon;
            dataGridViewCustomerOrder.Columns[0].MinimumWidth = 50;
            dataGridViewCustomerOrder.Columns[0].FillWeight = 100;

            dataGridViewCustomerOrder.Columns[1].Name = PC.ANZAHL;
            var viewColumn = dataGridViewCustomerOrder.Columns[PC.ANZAHL];
            if (viewColumn != null)
                viewColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCustomerOrder.Columns[1].DefaultCellStyle.ForeColor = Color.Maroon;
            dataGridViewCustomerOrder.Columns[1].MinimumWidth = 50;
            dataGridViewCustomerOrder.Columns[1].FillWeight = 100;

            dataGridViewCustomerOrder.Columns[2].Name = PC.NAME;
            dataGridViewCustomerOrder.Columns[2].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridViewCustomerOrder.Columns[2].DefaultCellStyle.Font = new Font(dataGridViewCustomerOrder.DefaultCellStyle.Font, FontStyle.Bold);
            dataGridViewCustomerOrder.Columns[2].DefaultCellStyle.ForeColor = Color.Crimson;
            dataGridViewCustomerOrder.Columns[2].MinimumWidth = 100;
            dataGridViewCustomerOrder.Columns[2].FillWeight = 200;

            dataGridViewCustomerOrder.Columns[3].Name = PC.GRÖSSE;
            var column = dataGridViewCustomerOrder.Columns[PC.GRÖSSE];
            if (column != null)
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCustomerOrder.Columns[3].DefaultCellStyle.ForeColor = Color.Crimson;
            dataGridViewCustomerOrder.Columns[3].MinimumWidth = 50;
            dataGridViewCustomerOrder.Columns[3].FillWeight = 100;

            dataGridViewCustomerOrder.Columns[4].Name = PC.PREIS;
            var dataGridViewColumn = dataGridViewCustomerOrder.Columns[PC.PREIS];
            if (dataGridViewColumn != null)
                dataGridViewColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCustomerOrder.Columns[4].DefaultCellStyle.Font = new Font(dataGridViewCustomerOrder.DefaultCellStyle.Font, FontStyle.Bold);
            dataGridViewCustomerOrder.Columns[4].DefaultCellStyle.Format = PC.CURRENCY;
            dataGridViewCustomerOrder.Columns[4].DefaultCellStyle.ForeColor = Color.Maroon;
            dataGridViewCustomerOrder.Columns[4].MinimumWidth = 50;
            dataGridViewCustomerOrder.Columns[4].FillWeight = 100;

            dataGridViewCustomerOrder.Focus();
            dataGridViewCustomerOrder.CurrentCell = dataGridViewCustomerOrder[0, dataGridViewCustomerOrder.Rows.Count - 1];
            dataGridViewCustomerOrder.CurrentCell.Selected = false;
            dataGridViewCustomerOrder.BeginEdit(true);
        }

        /// <summary>
        /// We are concerend here about the pizza items because only they
        /// can have diffrent sizes like single , jumbo etc...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCustomerOrder_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridViewCustomerOrder.IsCurrentCellDirty && dataGridViewCustomerOrder.CurrentCell.OwningColumn.Name == PC.GRÖSSE)
            {
                dataGridViewCustomerOrder.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        /// <summary>
        /// Calculate the Final Price for the customer bill.
        /// </summary>
        internal void CalculateTotalOrderPrice()
        {
            _serLogger.Log("Calculating TotalOrderPrice");
            var result = dataGridViewCustomerOrder.Rows.OfType<DataGridViewRow>().Where(row => row.Cells[PC.PREIS].Value != null)
                     .Sum(row => (decimal)row.Cells[PC.PREIS].Value);
            var totalPrice = result.ToString(PC.CURRENCY);
            if (txtTotalPrice.InvokeRequired)
            {
                txtTotalPrice.Invoke(new Action(() => txtTotalPrice.Text = totalPrice));
            }
            else
            {
                txtTotalPrice.Text = totalPrice;
            }
            _serLogger.Log($"Calculated TotalOrderPrice: {txtTotalPrice.Text}");
        }

        /// <summary>
        /// Only when the grid has focus and the enter key is pressed.
        /// Every valid press of the enter key will add the corressponding item in the
        /// Row of the datagrid with corresponding valid enteries.
        /// </summary>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            try
            {
                if (_bdataGridViewCustomerOrderHasFocus && keyData == Keys.Enter)
                {
                    if (!dataGridViewCustomerOrder.IsCurrentCellDirty)
                    {
                        _serLogger.Log("DataGridViewCustomerOrder current cell is not dirty");
                        return true;
                    }

                    var currentcell = dataGridViewCustomerOrder.CurrentCell;
                    if (string.IsNullOrEmpty(currentcell.EditedFormattedValue.ToString()))
                    {
                        _serLogger.Log("currentcell.EditedFormattedValue is not valid");
                        return true;
                    }

                    if (currentcell.OwningColumn.Name == PC.NUMMER)
                    {
                        _serLogger.Log("currentcell.OwningColumn.Name is Nummer");

                        int.TryParse(currentcell.EditedFormattedValue.ToString(), out int itemNumber);
                        _serLogger.Log($"Value of itemNumber:{itemNumber}");

                        //@todo: needs to be fixed, sync and async should not  be mixed.
                        var items = Task.Run(async () => await _serItems.SearchForAsync(x => x.ItemId == itemNumber, nameof(Items.listSubItems)).ConfigureAwait(false))
                                    .GetAwaiter().GetResult();

                        Items item = items?.FirstOrDefault();
                        //dataContext.Entry(item).Collection(g => g.LstSubItems).Load();
                        // dataContext.Entry(item).Reload();
                        if (item == null)
                        {
                            _serLogger.Log("item is null");
                            if (dataGridViewCustomerOrder.CurrentRow != null)
                                dataGridViewCustomerOrder.Rows.RemoveAt(dataGridViewCustomerOrder.CurrentRow.Index);
                            CalculateTotalOrderPrice();
                            dataGridViewCustomerOrder.CurrentCell = dataGridViewCustomerOrder[0, dataGridViewCustomerOrder.Rows.Count - 1];
                            return true;
                        }

                        _serLogger.Log("We prevent any update on the Datagrid here");
                        dataGridViewCustomerOrder.NotifyCurrentCellDirty(false);
                        Utilites.AdjustItemName(item.ItemName, out string trimmedArrItemsFirstElement,
                                                out IEnumerable<string> trimmedModArrItemsWithoutFirstElement,
                                                out IEnumerable<string> trimmedOrigArrItemsWithoutFirstElement);

                        if (!_lstSubItemsTuples.Any(t => t.Item1.Equals(itemNumber)))
                        {
                            _serLogger.Log("Adding values to _lsSubItemsTuples");
                            _lstSubItemsTuples.Add(Tuple.Create(itemNumber, trimmedOrigArrItemsWithoutFirstElement.Select(i => i.ToLower()), item.listSubItems.ToList()));
                        }
                        if (dataGridViewCustomerOrder.CurrentRow != null)
                        {
                            dataGridViewCustomerOrder.CurrentRow.Cells[PC.ANZAHL].Value = 1;//Initial nr. of items

                            var modArrItemsWithoutFirstElement = trimmedModArrItemsWithoutFirstElement as string[] ?? trimmedModArrItemsWithoutFirstElement.ToArray();
                            if (modArrItemsWithoutFirstElement.Length > 1)
                            {
                                // var myString = (modArrItemsWithoutFirstElement.Last() ?? string.Empty);
                                dataGridViewCustomerOrder.CurrentRow.Cells[PC.NAME].Value =
                                    trimmedArrItemsFirstElement +
                                     string.Join(PC.COMMAWITHSPACE, modArrItemsWithoutFirstElement.Take(modArrItemsWithoutFirstElement.Length - 1)).ToLower()
                                     + modArrItemsWithoutFirstElement.Last().ToLower();
                            }
                            else
                            {
                                dataGridViewCustomerOrder.CurrentRow.Cells[PC.NAME].Value = trimmedArrItemsFirstElement +
                                   (trimmedOrigArrItemsWithoutFirstElement.FirstOrDefault() ?? string.Empty).ToLower();
                            }
                            if (item.listSubItems.Any(y => y.SubItemCategory.Contains("Pizza")))
                            {
                                var comboBoxcell = new DataGridViewComboBoxCell();
                                comboBoxcell.Items.AddRange(PC.SINGLE, PC.JUMBO, PC.FAMILY, PC.PARTY);
                                comboBoxcell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing;
                                comboBoxcell.DropDownWidth = 5;// cell.OwningColumn.Width;
                                dataGridViewCustomerOrder.CurrentRow.Cells[PC.GRÖSSE] = comboBoxcell;
                            }
                            dataGridViewCustomerOrder.CurrentRow.Cells[PC.NUMMER].Value = currentcell.EditedFormattedValue;
                            dataGridViewCustomerOrder.CurrentRow.Cells[PC.GRÖSSE].Value = "Single";
                            _serLogger.Log("Now we allow update on the Datagrid here");
                            dataGridViewCustomerOrder.NotifyCurrentCellDirty(true);
                            //@todo: needs to be fixed, sync and async should not be mixed.
                            dataGridViewCustomerOrder.CurrentCell.OwningRow.Cells[PC.PREIS].Value =
                                Task.Run(async () => await _itPriceCalculation
                                .CalculateItemPriceAsync(currentcell.OwningRow).ConfigureAwait(false)).GetAwaiter().GetResult();
                            _serLogger.Log($"ItemNumber: {itemNumber} has Price: {dataGridViewCustomerOrder.CurrentCell.OwningRow.Cells[PC.PREIS].Value}");
                        }
                    }
                    _serLogger.Log("Set the Current cell");
                    dataGridViewCustomerOrder.CurrentCell = dataGridViewCustomerOrder[0, dataGridViewCustomerOrder.Rows.Count - 1];
                    return true;
                }
            }
            catch (Exception ex)
            {
                _serLogger.Log($" Failed to fill the grid with valid values: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// When the dataGridViewCustomerOrder looses its focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCustomerOrder_Leave(object sender, EventArgs e)
        {
            _bdataGridViewCustomerOrderHasFocus = false;
            dataGridViewCustomerOrder.ClearSelection();
        }

        /// <summary>
        /// When the dataGridViewCustomerOrder gains its focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridViewCustomerOrder_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            _bdataGridViewCustomerOrderHasFocus = true;
        }

        /// <summary>
        ////self explanatory
        /// </summary>
        /// <returns></returns>
        internal List<CustomerBills> PrepareCustomerBill()
        {
            _serLogger.Log($"Starting to print the Bill for the Customer{_currentCustomer.Id}");
            var listCustomerBills = new List<CustomerBills>();
            try
            {
                var resultset = _unOfWork.ExecuteRawSqlQuery(new ResultNextFreeRowId(), "select seq from sqlite_sequence where name = 'CustomerBills'");
                var nextRowId = (resultset?.Any() != true) ? 1 : resultset.First().Seq + 1;
                _serLogger.Log($"Next Valid RowId in the CustomerBills Table: {nextRowId}");

                foreach (DataGridViewRow row in dataGridViewCustomerOrder.Rows)
                {
                    if (row.Cells[PC.NUMMER].Value == null)
                        continue;
                    int.TryParse(row.Cells[PC.NUMMER].Value.ToString(), out int nNumber);
                    int.TryParse(row.Cells[PC.ANZAHL].Value.ToString(), out int nAnzahl);
                    var customerBill = new CustomerBills
                    {
                        CustomerBillItemId = nNumber,
                        CustomerBillNumberOfItems = nAnzahl,
                        CustomerBillItemSize = row.Cells[PC.GRÖSSE].Value.ToString(),
                        CustomerBillItemPrice = decimal.Parse(row.Cells[PC.PREIS].Value.ToString()),
                        CustomerBillItemName = row.Cells[PC.NAME].Value.ToString(),
                        CustomerRefId = _currentCustomer.Id,
                        CustomerBillNumber = nextRowId,
                        CustomerBillPrintTime = DateTime.Now
                    };
                    _serLogger.Log($"Save the Info for the current Customer with Billnumber{customerBill.CustomerBillNumber}");
                    listCustomerBills.Add(customerBill);
                }
            }
            catch (Exception ex)
            {
                _serLogger.Log($" Failed to PrepareCustomerBill the Customer Bill: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
            return listCustomerBills;
        }

        /// <summary>
        /// Opens the current Bill in the preview mode
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrintPreviewAsync_Click(object sender, EventArgs e)
        {
            try
            {
                var listCustomerBills = PrepareCustomerBill();
                if (listCustomerBills.Count == 0)
                    return;
                Cursor.Current = Cursors.WaitCursor;
                _serLogger.Log("Start print-view of the Bill now.");

                using (var frm = new FormCustomerOrderPrint(_serLogger))
                {
                    frm.PrepareToPrint(listCustomerBills, _currentCustomer, false);
                    frm.WindowState = FormWindowState.Maximized;
                    // frm.MaximumSizeChanged += (s, args) => Cursor.Current = Cursors.Cross ;
                    frm.ShowDialog();
                    _serLogger.Log("Print view of the Bill done now.");
                }
            }
            catch (Exception ex)
            {
                _serLogger.Log($" PrintPreview failed for the Customer Bill: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
        }

        public async Task SaveCustomerOrderIntoDb(List<CustomerBills> listCustomerBills)
        {
            try
            {
                _serLogger.Log($"Save the Info for the current Customer with Billnumber{listCustomerBills[0].CustomerBillNumber}");
                await _serCustomerBills.SaveAllAsync(listCustomerBills).ConfigureAwait(false);
                _serLogger.Log($"Successfully saved the Info for the current Customer order in Database:{_currentCustomer.Id}");
            }
            catch (Exception ex)
            {
                _serLogger.Log($" Failed to PrepareCustomerBill the Customer Bill: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
        }
    }
}
