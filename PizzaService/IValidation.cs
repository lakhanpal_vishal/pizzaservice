﻿
namespace PizzaService
{
    public interface IValidation
    {
        bool ValidateNumber(string strInput);
        bool ValidateText(string strInput);
    }
}
