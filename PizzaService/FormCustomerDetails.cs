﻿using PizzaService.BusinessLogic;
using PizzaService.PizzaServiceLocator;
using PizzaServiceDB.DbBase;
using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using PC = PizzaService.PizzaServiceConstants;

namespace PizzaService
{
    public partial class FormCustomerDetails : Form
    {
        #region FormCustomerDetails Members

        private readonly IValidation _validation;
        private readonly IPizService<Customer> _serCustomer;
        private readonly IPizService<Items> _serItems;
        private readonly Customer _customer;
        private readonly IItemPriceCalculation _itPriceCalculation;
        private readonly IPizService<ResultNextFreeRowId> _unOfWork;
        private readonly IPizService<CustomerBills> _serCustomerBills;
        private readonly IPizzaServiceLogger _serLogger;
        private bool _nonNumberEntered, _bCheckFields = true;
        private Customer _modifiedCustomer;
        private string _lastTxtTelefonOrCustomerId;
        private enum ECustomerReply { Yes, No, Cancel }
        private ECustomerReply _eCusReply = ECustomerReply.Yes;
        #endregion FormCustomerDetails Members

        #region FormCustomerDetails Methods

        public FormCustomerDetails(IValidation validation, IItemPriceCalculation itPriceCalculation,
            IPizService<Items> serItems, IPizService<Customer> serCustomer, Customer customer,
            IPizService<ResultNextFreeRowId> unOfWork, IPizService<CustomerBills> serCustomerBills,
            IPizzaServiceLogger serLogger)
        {
            InitializeComponent();
            _validation = validation;
            _serItems = serItems;
            _serCustomer = serCustomer;
            _customer = customer;
            _itPriceCalculation = itPriceCalculation;
            _unOfWork = unOfWork;
            _serCustomerBills = serCustomerBills;
            _serLogger = serLogger;

            //Test code to be used later for monthly , weekly, daily reports
            using (var dataContext = new PizzaServiceContext())
            {
                var total = from T1 in dataContext.CustomerBill
                            group T1 by new { T1.CustomerRefId, T1.CustomerBillNumber } into g
                            join T2 in dataContext.Customers on g.FirstOrDefault().CustomerRefId equals T2.Id
                            orderby (g.Sum(t3 => t3.CustomerBillItemPrice)) descending
                            select new
                            {
                                Column1 = T2.LastName,
                                Column2 = T2.FirstName,
                                BillNr = g.FirstOrDefault().CustomerBillNumber,
                                Date = g.FirstOrDefault().CustomerBillPrintTime,
                                Amount = g.Sum(t3 => t3.CustomerBillItemPrice)
                            };
                var h = total.ToList();
            }
        }

        /// <summary>
        /// Only digits are allowed in the input Text.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        private bool ValidateNumber(string strInput)
        {
            _serLogger.Log($"ValidateNumber: {strInput}");
            _bCheckFields = _validation.ValidateNumber(strInput);
            _serLogger.Log($"ValidateNumber result: {_bCheckFields}");
            return _bCheckFields;
        }

        /// <summary>
        /// Only digits or characters are allowed in the input Text.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        private void ValidateText(string strInput)
        {
            if (!_bCheckFields || string.IsNullOrEmpty(strInput))
            {
                throw new ArgumentException($"Invalid Text has been entered:{strInput}");
            }
            if (!_validation.ValidateText(strInput))
            {
                _bCheckFields = false;
            }
        }

        /// <summary>
        /// This will allow to add a new customer in the Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnInsert_ClickAsync(object sender, EventArgs e)
        {
            _bCheckFields = true;
            try
            {
                CheckFieldEnteries();
                if (!_bCheckFields)
                {
                    MessageBox.Show("Bitte korrigieren Sie Ihre Eingabe !!");
                    return;
                }

                _customer.Phone = txtTelefon.Text;
                _customer.FirstName = txtVorname.Text;
                _customer.LastName = txtNachName.Text;
                _customer.Address = txtStrasse.Text;
                _customer.City = txtOrt.Text;
                int.TryParse(txtPlz.Text, out int nPostalcode);
                if (nPostalcode > 0)
                {
                    _customer.Postalcode = nPostalcode;
                }

                await _serCustomer.SaveAsync(_customer).ConfigureAwait(false);

                ActivateOrderform(_customer);
                HideControls();
                chkSelfPicker.Visible = true;
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Insert Operation failed: {ex.Message} with InnerException: {ex.InnerException} with InnerException: {ex.InnerException} for Customer: {_customer.Id}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// //Verify the Enteries in the Text-Fields.
        /// </summary>
        private void CheckFieldEnteries()
        {
            ValidateNumber(txtTelefon.Text);
            ValidateText(txtOrt.Text);
        }

        /// <summary>
        /// // This Function modifies the existing record set in the 'Customer Table'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void btnModify_ClickAsync(object sender, EventArgs e)
        {
            _bCheckFields = true;
            try
            {
                CheckFieldEnteries();
                if (!_bCheckFields)
                {
                    MessageBox.Show("Bitte Korrigiern Sie Ihre Eingabe !!");
                    return;
                }
                await ModifyCustomerDataAsync().ConfigureAwait(false);
                ActivateOrderform(_modifiedCustomer);
                HideControls();
                chkSelfPicker.Visible = true;
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Modify Operation failed: {ex.Message} with InnerException: {ex.InnerException} with InnerException: {ex.InnerException} for Customer: {_modifiedCustomer.Id}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// Save the update changes into the DB.
        /// </summary>
        /// <returns></returns>
        private async Task ModifyCustomerDataAsync()
        {
            _modifiedCustomer.Address = txtStrasse.Text;
            _modifiedCustomer.City = txtOrt.Text;
            _modifiedCustomer.FirstName = txtVorname.Text;
            _modifiedCustomer.LastName = txtNachName.Text;
            _modifiedCustomer.Phone = txtTelefon.Text;
            int.TryParse(txtPlz.Text, out int nPostalcode);
            if (nPostalcode > 0)
                _modifiedCustomer.Postalcode = nPostalcode;

            await _serCustomer.UpdateAsync(_modifiedCustomer).ConfigureAwait(false);
        }

        /// <summary>
        /// self evident
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCustomerDetails_Load(object sender, EventArgs e)
        {
            HideControls();
            if (txtTelefon != null)
                txtTelefon.KeyPress += CheckTelefonKeysAsync;
        }

        /// <summary>
        /// Hide the specific controls
        /// </summary>
        private void HideControls()
        {
            lblTelefon.Location = new Point(PC.HIDE_LOC_X, PC.HIDE_LOC_Y);
            txtTelefon.Location = new Point(PC.HIDE_LOC_X, PC.HIDE_LOC_Y + PC.HIDE_LOC_DELTA_Y);
            chkSelfPicker.Location = new Point(PC.HIDE_LOC_X, PC.HIDE_LOC_Y + (2 * PC.HIDE_LOC_DELTA_Y));
            lblVorname.Visible = false;
            txtVorname.Visible = false;
            lblNachname.Visible = false;
            txtNachName.Visible = false;
            lblStrasse.Visible = false;
            txtStrasse.Visible = false;
            lblOrt.Visible = false;
            txtOrt.Visible = false;
            lblPlz.Visible = false;
            txtPlz.Visible = false;
            btnInsert.Visible = false;
            btnModify.Visible = false;
            btnCancel.Visible = false;
        }

        /// <summary>
        /// Display and Move the Controls to specific location
        /// </summary>
        private void ShowAndMoveControls()
        {
            lblTelefon.Location = new Point(PC.SHOW_LOC_X, PC.SHOW_LOC_Y);
            txtTelefon.Location = new Point(PC.SHOW_LOC_X + PC.SHOW_LOC_DELTA_X, PC.SHOW_LOC_Y);
            chkSelfPicker.Visible = false;
            lblVorname.Visible = true;
            txtVorname.Visible = true;
            lblNachname.Visible = true;
            txtNachName.Visible = true;
            lblStrasse.Visible = true;
            txtStrasse.Visible = true;
            lblOrt.Visible = true;
            txtOrt.Visible = true;
            lblPlz.Visible = true;
            txtPlz.Visible = true;
        }

        /// <summary>
        /// Once the telefon number is entered and if this is a valid input
        /// appropriate action will be taken.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void CheckTelefonKeysAsync(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == Convert.ToChar(Keys.Enter) && ValidateNumber(txtTelefon.Text))
                {
                    var openForm = Application.OpenForms["FormCustomerOrder"];
                    // We cannot open simultaneously more than one  "FormCustomerOrder" from.
                    if (openForm != null)
                        return;

                    Customer customer = await Task.Run(() => RetrieveCurrentCustomerFromDbAsync());
                    if (customer != null && _eCusReply == ECustomerReply.Cancel)
                        return;

                    _lastTxtTelefonOrCustomerId = txtTelefon.Text;

                    //Modify the existing DataRecord.
                    if (_eCusReply == ECustomerReply.No && customer != null)
                    {
                        txtTelefon.Text = customer.Phone;
                        txtVorname.Text = customer.FirstName;
                        txtNachName.Text = customer.LastName;
                        txtStrasse.Text = customer.Address;
                        txtOrt.Text = customer.City;
                        if (customer.Postalcode > 0)
                            txtPlz.Text = customer.Postalcode.ToString();
                        _modifiedCustomer = customer;
                        ShowAndMoveControls();
                        btnModify.Visible = true;
                        btnInsert.Visible = false;
                        btnCancel.Visible = true;
                        btnModify.Location = new Point(PC.BTN_LOC_X, PC.BTN_LOC_Y);
                    }
                    // Add the new DataRecord in the 'Customer' Table.
                    else if (customer == null)
                    {
                        ShowAndMoveControls();
                        txtVorname.Text = string.Empty;
                        txtNachName.Text = string.Empty;
                        txtStrasse.Text = string.Empty;
                        txtOrt.Text = string.Empty;
                        txtPlz.Text = string.Empty;
                        btnInsert.Visible = true;
                        btnModify.Visible = false;
                        btnCancel.Visible = true;
                        btnInsert.Location = new Point(PC.BTN_LOC_X, PC.BTN_LOC_Y);
                    }
                    else //Just open the OrderForm as the customer exists in the DB....
                    {
                        ActivateOrderform(customer);
                    }
                }
            }
            catch (Exception ex)
            {
                var strErrMsg = $" Operation failed: {ex.Message} with InnerException: {ex.InnerException} <-> {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// only digits between 0 and 9 are allowed.
        /// Copied from MSDN :)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPlz_KeyDown(object sender, KeyEventArgs e)
        {
            // Initialize the flag to false.
            _nonNumberEntered = false;

            // Determine whether the keystroke is a number from the top of the keyboard.
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad.
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace.
                    _nonNumberEntered |= e.KeyCode != Keys.Back;
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPlz_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Check for the flag being set in the KeyDown event.
            e.Handled |= _nonNumberEntered;
        }

        /// <summary>
        /// Here we display the CustomerOrder form.
        /// </summary>
        /// <param name="cus"></param>
        private void ActivateOrderform(Customer cus)
        {
            var frmCustomerOrder = new FormCustomerOrder(_itPriceCalculation, _serItems, _unOfWork, _serCustomerBills, _serLogger);
            Action<Customer> customer = frmCustomerOrder.SaveCustomerData;
            customer(cus);
            frmCustomerOrder.StartPosition = FormStartPosition.CenterScreen;
            Hide();
            frmCustomerOrder.Closed += (s, args) => Show();
            frmCustomerOrder.Show();
        }

        /// <summary>
        /// Here we  create a Abholer and then get ready to
        /// accept his order.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void chkSelfPicker_ClickAsync(object sender, EventArgs e)
        {
            if (chkSelfPicker.CheckState != CheckState.Checked)
                return;
            _customer.FirstName = PC.ABHOLER;
            _customer.Phone = string.Empty;
            await _serCustomer.SaveAsync(_customer).ConfigureAwait(false);
            ActivateOrderform(_customer);
            chkSelfPicker.CheckState = CheckState.Unchecked; //Reset
        }

        /// <summary>
        /// In Edit/Insert Mode , when the user presses the cancel button,
        /// return back to the settings of the startup form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            HideControls();
            chkSelfPicker.Location = new Point(PC.CHK_BTN_LOC_X, PC.CHK_BTN_LOC_Y);
            chkSelfPicker.Visible = true;
            txtTelefon.Text = _lastTxtTelefonOrCustomerId;
        }

        /// <summary>
        /// Self Evident
        /// </summary>
        private async Task<Customer> RetrieveCurrentCustomerFromDbAsync()
        {
            Customer currentCustomer = null;
            try
            {
                //@Todo :First Db call takes a long time. add a splash screen
                //dataContext.Database.Log = s => Debug.Write(s);

                var customers = await _serCustomer.SearchForAsync(b => b.Phone == txtTelefon.Text || b.Id.ToString() == txtTelefon.Text).ConfigureAwait(false);
                var enumerable = customers as Customer[] ?? customers.ToArray();
                if (enumerable.Length > 0)
                {
                    currentCustomer = enumerable[0];

                    var dlgResult =
                        MessageBox.Show(currentCustomer.FirstName + Environment.NewLine + currentCustomer.LastName
                                        + Environment.NewLine + currentCustomer.Phone,
                            "Customer Details",
                            MessageBoxButtons.YesNoCancel,
                            MessageBoxIcon.Question);

                    switch (dlgResult)
                    {
                        case DialogResult.Cancel:
                            {
                                _eCusReply = ECustomerReply.Cancel;
                                break;
                            }
                        case DialogResult.No:
                            {
                                _eCusReply = ECustomerReply.No;
                                break;
                            }
                        default:
                            {
                                _eCusReply = ECustomerReply.Yes;
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Operation failed: {ex.Message} with InnerException: {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
            return currentCustomer;
        }
    }
    #endregion FormCustomerDetails Methods
}
