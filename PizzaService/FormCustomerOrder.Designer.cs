﻿namespace PizzaService
{
    partial class FormCustomerOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblStrasse0 = new System.Windows.Forms.Label();
            this.lblOrt0 = new System.Windows.Forms.Label();
            this.lblPlz0 = new System.Windows.Forms.Label();
            this.lblName0 = new System.Windows.Forms.Label();
            this.lblTel0 = new System.Windows.Forms.Label();
            this.lblOrt = new System.Windows.Forms.Label();
            this.lblStrasse = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.lblPlz = new System.Windows.Forms.Label();
            this.tableLayoutCusDetailsPanel = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewCustomerOrder = new System.Windows.Forms.DataGridView();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnPrintPreview = new System.Windows.Forms.Button();
            this.txtTotalPrice = new System.Windows.Forms.TextBox();
            this.lblSumme = new System.Windows.Forms.Label();
            this.tableLayoutPrintPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPaymentPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutGridPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutCusDetailsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomerOrder)).BeginInit();
            this.tableLayoutPrintPanel.SuspendLayout();
            this.tableLayoutPaymentPanel.SuspendLayout();
            this.tableLayoutGridPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStrasse0
            // 
            this.lblStrasse0.AutoSize = true;
            this.lblStrasse0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStrasse0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStrasse0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblStrasse0.Location = new System.Drawing.Point(3, 58);
            this.lblStrasse0.Name = "lblStrasse0";
            this.lblStrasse0.Size = new System.Drawing.Size(390, 29);
            this.lblStrasse0.TabIndex = 11;
            this.lblStrasse0.Text = "Straße:";
            this.lblStrasse0.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblOrt0
            // 
            this.lblOrt0.AutoSize = true;
            this.lblOrt0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOrt0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrt0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblOrt0.Location = new System.Drawing.Point(3, 87);
            this.lblOrt0.Name = "lblOrt0";
            this.lblOrt0.Size = new System.Drawing.Size(390, 29);
            this.lblOrt0.TabIndex = 12;
            this.lblOrt0.Text = "Ort:";
            this.lblOrt0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPlz0
            // 
            this.lblPlz0.AutoSize = true;
            this.lblPlz0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPlz0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlz0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblPlz0.Location = new System.Drawing.Point(3, 116);
            this.lblPlz0.Name = "lblPlz0";
            this.lblPlz0.Size = new System.Drawing.Size(390, 33);
            this.lblPlz0.TabIndex = 13;
            this.lblPlz0.Text = "Plz:";
            this.lblPlz0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName0
            // 
            this.lblName0.AutoSize = true;
            this.lblName0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblName0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblName0.Location = new System.Drawing.Point(3, 29);
            this.lblName0.Name = "lblName0";
            this.lblName0.Size = new System.Drawing.Size(390, 29);
            this.lblName0.TabIndex = 10;
            this.lblName0.Text = "Name:";
            this.lblName0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTel0
            // 
            this.lblTel0.AutoSize = true;
            this.lblTel0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTel0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTel0.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblTel0.Location = new System.Drawing.Point(3, 0);
            this.lblTel0.Name = "lblTel0";
            this.lblTel0.Size = new System.Drawing.Size(390, 29);
            this.lblTel0.TabIndex = 9;
            this.lblTel0.Text = "KundenNr/Telefon:";
            this.lblTel0.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOrt
            // 
            this.lblOrt.AutoSize = true;
            this.lblOrt.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblOrt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrt.ForeColor = System.Drawing.Color.Crimson;
            this.lblOrt.Location = new System.Drawing.Point(399, 87);
            this.lblOrt.Name = "lblOrt";
            this.lblOrt.Size = new System.Drawing.Size(390, 29);
            this.lblOrt.TabIndex = 11;
            this.lblOrt.Text = "Ort";
            this.lblOrt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblStrasse
            // 
            this.lblStrasse.AutoSize = true;
            this.lblStrasse.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblStrasse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblStrasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStrasse.ForeColor = System.Drawing.Color.Crimson;
            this.lblStrasse.Location = new System.Drawing.Point(399, 58);
            this.lblStrasse.Name = "lblStrasse";
            this.lblStrasse.Size = new System.Drawing.Size(390, 29);
            this.lblStrasse.TabIndex = 10;
            this.lblStrasse.Text = "Strasse";
            this.lblStrasse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.AllowDrop = true;
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.Crimson;
            this.lblName.Location = new System.Drawing.Point(399, 29);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(390, 29);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblTelefon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefon.ForeColor = System.Drawing.Color.Crimson;
            this.lblTelefon.Location = new System.Drawing.Point(399, 0);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(390, 29);
            this.lblTelefon.TabIndex = 7;
            this.lblTelefon.Text = "Telefon";
            this.lblTelefon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPlz
            // 
            this.lblPlz.AutoSize = true;
            this.lblPlz.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblPlz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPlz.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlz.ForeColor = System.Drawing.Color.Crimson;
            this.lblPlz.Location = new System.Drawing.Point(399, 116);
            this.lblPlz.Name = "lblPlz";
            this.lblPlz.Size = new System.Drawing.Size(390, 33);
            this.lblPlz.TabIndex = 14;
            this.lblPlz.Text = "Plz";
            this.lblPlz.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutCusDetailsPanel
            // 
            this.tableLayoutCusDetailsPanel.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.tableLayoutCusDetailsPanel.ColumnCount = 2;
            this.tableLayoutCusDetailsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutCusDetailsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblPlz0, 0, 4);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblPlz, 1, 4);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblTelefon, 1, 0);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblOrt0, 0, 3);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblName0, 0, 1);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblName, 1, 1);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblOrt, 1, 3);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblStrasse0, 0, 2);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblStrasse, 1, 2);
            this.tableLayoutCusDetailsPanel.Controls.Add(this.lblTel0, 0, 0);
            this.tableLayoutCusDetailsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutCusDetailsPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutCusDetailsPanel.Name = "tableLayoutCusDetailsPanel";
            this.tableLayoutCusDetailsPanel.RowCount = 5;
            this.tableLayoutCusDetailsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutCusDetailsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutCusDetailsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutCusDetailsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutCusDetailsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutCusDetailsPanel.Size = new System.Drawing.Size(792, 149);
            this.tableLayoutCusDetailsPanel.TabIndex = 16;
            // 
            // dataGridViewCustomerOrder
            // 
            this.dataGridViewCustomerOrder.AllowUserToOrderColumns = true;
            this.dataGridViewCustomerOrder.AllowUserToResizeColumns = false;
            this.dataGridViewCustomerOrder.AllowUserToResizeRows = false;
            this.dataGridViewCustomerOrder.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCustomerOrder.BackgroundColor = System.Drawing.Color.PaleGoldenrod;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCustomerOrder.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCustomerOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewCustomerOrder.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridViewCustomerOrder.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewCustomerOrder.Name = "dataGridViewCustomerOrder";
            this.dataGridViewCustomerOrder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridViewCustomerOrder.Size = new System.Drawing.Size(786, 291);
            this.dataGridViewCustomerOrder.TabIndex = 1;
            this.dataGridViewCustomerOrder.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCustomerOrder_CellEnter);
            this.dataGridViewCustomerOrder.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewCustomerOrder_CellValueChangedAsync);
            this.dataGridViewCustomerOrder.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridViewCustomerOrder_CurrentCellDirtyStateChanged);
            this.dataGridViewCustomerOrder.Leave += new System.EventHandler(this.dataGridViewCustomerOrder_Leave);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.Color.Crimson;
            this.btnPrint.Location = new System.Drawing.Point(407, 3);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(382, 55);
            this.btnPrint.TabIndex = 5;
            this.btnPrint.Text = "Drucken";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_ClickAsync);
            // 
            // btnPrintPreview
            // 
            this.btnPrintPreview.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.btnPrintPreview.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrintPreview.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintPreview.ForeColor = System.Drawing.Color.Crimson;
            this.btnPrintPreview.Location = new System.Drawing.Point(3, 3);
            this.btnPrintPreview.Name = "btnPrintPreview";
            this.btnPrintPreview.Size = new System.Drawing.Size(398, 55);
            this.btnPrintPreview.TabIndex = 4;
            this.btnPrintPreview.Text = "Vorschau";
            this.btnPrintPreview.UseVisualStyleBackColor = false;
            this.btnPrintPreview.Click += new System.EventHandler(this.btnPrintPreviewAsync_Click);
            // 
            // txtTotalPrice
            // 
            this.txtTotalPrice.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtTotalPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotalPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalPrice.ForeColor = System.Drawing.Color.Maroon;
            this.txtTotalPrice.Location = new System.Drawing.Point(693, 3);
            this.txtTotalPrice.Name = "txtTotalPrice";
            this.txtTotalPrice.Size = new System.Drawing.Size(96, 29);
            this.txtTotalPrice.TabIndex = 3;
            this.txtTotalPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblSumme
            // 
            this.lblSumme.AutoSize = true;
            this.lblSumme.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.lblSumme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSumme.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSumme.ForeColor = System.Drawing.Color.Crimson;
            this.lblSumme.Location = new System.Drawing.Point(3, 0);
            this.lblSumme.Name = "lblSumme";
            this.lblSumme.Size = new System.Drawing.Size(684, 38);
            this.lblSumme.TabIndex = 15;
            this.lblSumme.Text = "Summe:";
            this.lblSumme.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPrintPanel
            // 
            this.tableLayoutPrintPanel.ColumnCount = 2;
            this.tableLayoutPrintPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.13636F));
            this.tableLayoutPrintPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.86364F));
            this.tableLayoutPrintPanel.Controls.Add(this.btnPrint, 1, 0);
            this.tableLayoutPrintPanel.Controls.Add(this.btnPrintPreview, 0, 0);
            this.tableLayoutPrintPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPrintPanel.Location = new System.Drawing.Point(0, 484);
            this.tableLayoutPrintPanel.Name = "tableLayoutPrintPanel";
            this.tableLayoutPrintPanel.RowCount = 1;
            this.tableLayoutPrintPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPrintPanel.Size = new System.Drawing.Size(792, 61);
            this.tableLayoutPrintPanel.TabIndex = 17;
            // 
            // tableLayoutPaymentPanel
            // 
            this.tableLayoutPaymentPanel.ColumnCount = 2;
            this.tableLayoutPaymentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 87.12122F));
            this.tableLayoutPaymentPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.87879F));
            this.tableLayoutPaymentPanel.Controls.Add(this.txtTotalPrice, 1, 0);
            this.tableLayoutPaymentPanel.Controls.Add(this.lblSumme, 0, 0);
            this.tableLayoutPaymentPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPaymentPanel.Location = new System.Drawing.Point(0, 446);
            this.tableLayoutPaymentPanel.Name = "tableLayoutPaymentPanel";
            this.tableLayoutPaymentPanel.RowCount = 1;
            this.tableLayoutPaymentPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPaymentPanel.Size = new System.Drawing.Size(792, 38);
            this.tableLayoutPaymentPanel.TabIndex = 18;
            // 
            // tableLayoutGridPanel
            // 
            this.tableLayoutGridPanel.ColumnCount = 1;
            this.tableLayoutGridPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutGridPanel.Controls.Add(this.dataGridViewCustomerOrder, 0, 0);
            this.tableLayoutGridPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutGridPanel.Location = new System.Drawing.Point(0, 149);
            this.tableLayoutGridPanel.Name = "tableLayoutGridPanel";
            this.tableLayoutGridPanel.RowCount = 1;
            this.tableLayoutGridPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutGridPanel.Size = new System.Drawing.Size(792, 297);
            this.tableLayoutGridPanel.TabIndex = 19;
            // 
            // FormCustomerOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.ClientSize = new System.Drawing.Size(792, 545);
            this.Controls.Add(this.tableLayoutGridPanel);
            this.Controls.Add(this.tableLayoutPaymentPanel);
            this.Controls.Add(this.tableLayoutPrintPanel);
            this.Controls.Add(this.tableLayoutCusDetailsPanel);
            this.KeyPreview = true;
            this.Name = "FormCustomerOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bestellung";
            this.Load += new System.EventHandler(this.FormCustomerOrder_Load);
            this.tableLayoutCusDetailsPanel.ResumeLayout(false);
            this.tableLayoutCusDetailsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCustomerOrder)).EndInit();
            this.tableLayoutPrintPanel.ResumeLayout(false);
            this.tableLayoutPaymentPanel.ResumeLayout(false);
            this.tableLayoutPaymentPanel.PerformLayout();
            this.tableLayoutGridPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblStrasse0;
        private System.Windows.Forms.Label lblOrt0;
        private System.Windows.Forms.Label lblPlz0;
        private System.Windows.Forms.Label lblName0;
        private System.Windows.Forms.Label lblTel0;
        private System.Windows.Forms.Label lblOrt;
        private System.Windows.Forms.Label lblStrasse;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.Label lblPlz;
        private System.Windows.Forms.TableLayoutPanel tableLayoutCusDetailsPanel;
        private System.Windows.Forms.DataGridView dataGridViewCustomerOrder;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnPrintPreview;
        private System.Windows.Forms.TextBox txtTotalPrice;
        private System.Windows.Forms.Label lblSumme;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPrintPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPaymentPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutGridPanel;
    }
}