﻿using System.Text.RegularExpressions;

namespace PizzaService
{
    public class Validation : IValidation
    {
        public bool ValidateNumber(string strInput) =>
            long.TryParse(strInput, out var _);

        /// <summary>
        /// Only digits, characters, blankspace, slashes and hyphens are allowed in the input Text.
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public bool ValidateText(string strInput) =>
             Regex.IsMatch(strInput, @"^[a-zA-ZÄäÖöÜüß0-9\s-_\\//]*$");
    }
}
