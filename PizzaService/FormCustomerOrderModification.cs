﻿using PizzaService.BusinessLogic;
using PizzaServiceDB.DbBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using PC = PizzaService.PizzaServiceConstants;

namespace PizzaService
{
    public partial class FormCustomerOrderModification : Form
    {
        private DataGridViewRow _modifiedCurrentRow;
        private IEnumerable<string> _initialItemParts;
        private readonly List<string> _lstInitialSubItemFreeItemParts;
        private readonly IItemPriceCalculation _itPriceCalculation;
        private readonly IPizzaServiceLogger _serLogger;

        private int _initialItemPartsCount;
        private bool _bUpdateLstOptionFreeSubItems;
        private List<SubItems> _lstSubItems;

        public FormCustomerOrderModification(IItemPriceCalculation itPriceCalculation, IPizzaServiceLogger serLogger)
        {
            _itPriceCalculation = itPriceCalculation;
            _serLogger = serLogger;
            _lstInitialSubItemFreeItemParts = new List<string>();
            InitializeComponent();
        }

        /// <summary>
        /// This function allows to modify(add or delete) the contents(i.e.SubItem) the current Item
        /// </summary>
        /// <param name="currentRow"></param>
        /// <param name="lstsubItemsTuples"></param>
        public void ModifyCurrentRowContents(DataGridViewRow currentRow,
            List<Tuple<int, IEnumerable<string>, List<SubItems>>> lstsubItemsTuples)
        {
            _modifiedCurrentRow = currentRow;
            int.TryParse(currentRow.Cells[PC.NUMMER].Value.ToString(), out int itemNumber);
            _serLogger.Log($"ItemNumber:{itemNumber}");

            var tplItem = lstsubItemsTuples?.FirstOrDefault(t => t.Item1 == itemNumber);

            if (tplItem != null)
            {
                _serLogger.Log("tplItem is not null");

                _lstSubItems = tplItem.Item3;//List of all the available sub-items
                _initialItemParts = tplItem.Item2;

                if (_initialItemParts.Any())
                {
                    var myString = (_initialItemParts.Last() ?? string.Empty).Trim();
                    var temp = (myString.StartsWith(PC.UND, StringComparison.Ordinal) ? myString.Replace(PC.UND, string.Empty) : string.Empty).Trim();
                    //@Todo:Needs to check, this is code with "if condition" is used anymore.
                    if (!string.IsNullOrEmpty(temp))
                    {
                        var allButLast = _initialItemParts.TakeWhile((_, i) => i < _initialItemParts.Count() - 1);
                        var myList = allButLast.ToList();
                        myList.Add(temp);
                        _initialItemParts = myList;
                    }

                    _initialItemPartsCount = tplItem.Item2.Count();//List of all the current subitems, that are part of Item.
                    _serLogger.Log($"InitialItemPartsCount : {_initialItemPartsCount}");
                }
                var itemText = (currentRow.Cells[PC.NAME].Value ?? string.Empty).ToString();
                _serLogger.Log($"ItemName : {itemText}");
                var stringSeparators = Utilites.GetSeprators();
                var splitItemText = itemText.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries)
                                                       .Select(i => i.Trim()).ToArray();
                // create unselected subitems which do not start with '*' or '+'
                var unSelectedFreeSubItems = splitItemText.Where(f => f != null && (f.StartsWith(PC.ASTERIX, StringComparison.Ordinal)
                || f.StartsWith(PC.PLUS, StringComparison.Ordinal))).Select(f => f.Substring(1)).ToList();

                PrepareOptionsListBox(tplItem.Item3, unSelectedFreeSubItems);

                _serLogger.Log("unSelectedFreeSubItems done");
                lblModifyingItem.Text = splitItemText[0];
                _serLogger.Log($"Main Item :{lblModifyingItem.Text}");

                var splitItemTextWithoutFirstPart = splitItemText.Skip(1).ToArray<object>();//.OrderByDescending(b => !b.StartsWith(PC.ASTERIX));
                lstBoxCurrentSubItems.Items.AddRange(splitItemTextWithoutFirstPart);
                _serLogger.Log("lstBoxCurrentSubItems.Items.AddRange done");
            }

            UpdateSelectionListBoxCurrentSubItems();
            _serLogger.Log("UpdateSelectionListBoxCurrentSubItems done");

            DisplayAddedSubItemsCount();
            _serLogger.Log("DisplayAddedSubItemsCount done");

            txtPrice.Text = ((decimal)currentRow.Cells[PC.PREIS].Value).ToString(PC.CURRENCY);
            _serLogger.Log($"New Price:{txtPrice.Text}");

            _bUpdateLstOptionFreeSubItems = true;
        }

        /// <summary>
        /// Here we prepare the ListBoxes for the optional 'Paid' and optional 'free' subitems
        /// </summary>
        /// <param name="lstSubItems"></param>
        /// <param name="unselectedFreeSubItems"></param>
        private void PrepareOptionsListBox(List<SubItems> lstSubItems, List<string> unselectedFreeSubItems)
        {
            foreach (var subItem in lstSubItems)
            {
                var trimmedSubItemText = subItem.SubItemText.ToLower().Split(',').Select(p => p.Trim()).ToArray<object>();
                //Add paid optional subitems here
                lstBoxOptionPaidSubItems.Items.AddRange(trimmedSubItemText);

                if (string.IsNullOrEmpty(subItem.SubItemFreeText))
                    continue;

                var trimmedSubItemFreeText = subItem.SubItemFreeText.ToLower().Split(',').Select(p => p.Trim());
                var arrSubItemFreeText = trimmedSubItemFreeText as string[] ?? trimmedSubItemFreeText.ToArray();

                _lstInitialSubItemFreeItemParts.AddRange(arrSubItemFreeText);
                //Add free optional subitems here
                lstBoxOptionFreeSubItems.Items.AddRange(arrSubItemFreeText.ToArray<object>());
                //select the lstBoxOptionFreeSubItems accordingly
                var newUnSelectedFreeSubItems = arrSubItemFreeText.Where(x => x != null && !unselectedFreeSubItems.Contains(x)).ToList();
                newUnSelectedFreeSubItems.ForEach(i => lstBoxOptionFreeSubItems.SelectedItem = i);
            }
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstBoxOptionFreeSubItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            //avoid all updates till the List box is completely filled or ready
            if (!_bUpdateLstOptionFreeSubItems)
                return;

            var lstOptionFreeSubItems = new List<string>();
            lstOptionFreeSubItems.AddRange(lstBoxOptionFreeSubItems.Items.OfType<string>().Select(x => x));

            var unSelectedFreeSubItems = lstOptionFreeSubItems.Where(x => !lstBoxOptionFreeSubItems.SelectedItems.Contains(x));
            var freeSubItems = unSelectedFreeSubItems as string[] ?? unSelectedFreeSubItems.ToArray();

            IEnumerable<string> MySubItemsList(IEnumerable<string> p, string c)
            {
                var lstSubItemCategory = _lstSubItems.Where(i => i.SubItemCategory.ToLower().Equals(c)).SelectMany(i => i.SubItemFreeText.ToLower().Split(','));

                var lstPresentElements = p.Where(i => lstSubItemCategory.Contains(i));
                if (lstPresentElements.Count() > 1)
                {
                    var newToAdd = lstPresentElements.Where(x => !lstBoxCurrentSubItems.Items.OfType<string>().Contains(PC.PLUS + x));
                    return p.Except(lstPresentElements).Concat(newToAdd);
                }

                return p;
            }

            var retFreeSubItems = MySubItemsList(freeSubItems, "salat");
            freeSubItems = MySubItemsList(retFreeSubItems, "pasta").ToArray();

            var lstPizzaFreeSubItems = new List<string>();
            var lstNonPizzaFreeSubItems = new List<string>();

            foreach (var it in freeSubItems)
            {
                var item = _lstSubItems.Find(s => s.SubItemFreeText.ToLower().Contains(it));
                if (item == null)
                    continue;
                //FreeSubItems starting with '*' will be removed of current item and
                if (item.SubItemCategory.IndexOf("pizza", StringComparison.OrdinalIgnoreCase) >= 0)
                    lstPizzaFreeSubItems.Add(PC.ASTERIX + it);
                //FreeSubItems starting with '+' will be part of current item and
                //is free of cost
                else
                    lstNonPizzaFreeSubItems.Add(PC.PLUS + it);
            }

            var unSelectedNonPizzaFreeSubItems = string.Join(PC.COMMAWITHSPACE, lstNonPizzaFreeSubItems);
            var unSelectedPizzaFreeSubItems = string.Join(PC.COMMAWITHSPACE, lstPizzaFreeSubItems);
            unSelectedNonPizzaFreeSubItems += PC.COMMAWITHSPACE + unSelectedPizzaFreeSubItems;

            UpdateListBoxCurrentSubItems(unSelectedNonPizzaFreeSubItems.Split(',').
                         Select(p => p.Trim()).ToArray(), false, true);

            _lstInitialSubItemFreeItemParts.
               Where(x => !freeSubItems.Contains(x)).ToList()
               .ForEach(i => lstBoxOptionFreeSubItems.SelectedItem = i);
        }

        /// <summary>
        /// This adds the current selected paid subitem(s) to the
        /// current Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnAddSubItem_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                var items = lstBoxOptionPaidSubItems.SelectedItems.OfType<string>().Distinct()
                .Where(x => x != null && !lstBoxCurrentSubItems.Items.Contains(x));

                UpdateListBoxCurrentSubItems(items.ToArray(), false);
                await ExecuteCommonCode().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                var strErrMsg = $"AddSubItem Operation failed: {ex.Message} with InnerException: {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// Remove subitem from the current Item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnRemoveSubItem_ClickAsync(object sender, EventArgs e)
        {
            var listBoxCurrentSubSelectedItems = lstBoxCurrentSubItems.SelectedItems;
            try
            {
                for (var i = listBoxCurrentSubSelectedItems.Count - 1; i >= 0; i--)
                {
                    var item = listBoxCurrentSubSelectedItems[i];
                    lstBoxCurrentSubItems.Items.Remove(item);
                    if (!lstBoxOptionPaidSubItems.Items.Contains(item))
                        lstBoxOptionPaidSubItems.Items.Add(item);
                }

                UpdateSelectionListBoxCurrentSubItems();
                await ExecuteCommonCode().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Remove SubItem Operation failed: {ex.Message} with InnerException: {ex.InnerException} for {lstBoxCurrentSubItems.SelectedItems.OfType<string>()}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// This gives the user to add manually the subitems(free or paid)
        /// in case, they are not available in the current database
        /// When the checkbox is checked, all the comma seprated subitems will be
        /// added with '+' sign to make it a non paid subitem.
        /// Similarly to remove a subitem add a '*' to comma seprated subitem.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void BtnExtraSubItems_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                _serLogger.Log($"Extra SubItems added :{txtExtraSubItems.Text}");
                var tempText = txtExtraSubItems.Text.Trim();
                if (string.IsNullOrEmpty(tempText))
                    return;
                //Remove multiple occurrence of the ('+') from subitem .
                var splitExtraSubItems = tempText.Split(',').Select(p => p.TrimStart('+').Trim()).Distinct().ToArray();
                if (checkBoxFreeSubItems.Checked)// Free items like spice, salt, dressing  etc....
                    splitExtraSubItems = splitExtraSubItems.Select(x => $"+{x}").Distinct().ToArray();

                var newExtraSubItems = splitExtraSubItems.Where(x => !lstBoxCurrentSubItems.Items.OfType<string>().Contains(x)).ToArray();
                _serLogger.Log($"New Extra SubItems added :{newExtraSubItems}");

                UpdateListBoxCurrentSubItems(newExtraSubItems, checkBoxFreeSubItems.Checked);
                await ExecuteCommonCode().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Adding Extra Operation failed: {ex.Message} with InnerException: {ex.InnerException} for {txtExtraSubItems.Text}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// Here we Prepare our list of original current subitems with the new paid
        /// subitems along with free subitems. Moreover the newly added paid subitems
        /// will be selected in the lstBoxCurrentSubItems.
        /// ToDo :: logging needs to checked again as it does not print readable results.
        /// </summary>
        /// <param name="arrNewExtraSubItems"></param>
        /// <param name="bCheckBox"></param>
        /// <param name="bUpdateWithSelection"></param>
        private void UpdateListBoxCurrentSubItems(string[] arrNewExtraSubItems,
            bool bCheckBox, bool bUpdateWithSelection = false)
        {
            try
            {
                var currentSubItems = lstBoxCurrentSubItems.Items.OfType<string>();
                //Remove empty subitems, incase they are present
                arrNewExtraSubItems = arrNewExtraSubItems.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                _serLogger.Log($"arrNewExtraSubItems :{arrNewExtraSubItems.Take(1)}");

                // Itemlist without free any free Items
                var subItems = currentSubItems as string[] ?? currentSubItems.ToArray();

                var paidItemlist = subItems.Where(x => !x.StartsWith(PC.ASTERIX, StringComparison.Ordinal) && !x.StartsWith(PC.PLUS, StringComparison.Ordinal)).ToList();
                _serLogger.Log($"paidItemlist :{paidItemlist}");

                // Itemlist with free Items like salt, spice, dressing etc....
                var freeItemlist = subItems.Where(x => x.StartsWith(PC.PLUS, StringComparison.Ordinal)).ToList();
                _serLogger.Log($"freeItemlist :{freeItemlist}");

                // SubItemFreelist with to be deleted Items..
                var subItemFreelist = subItems.Where(x => x.StartsWith(PC.ASTERIX, StringComparison.Ordinal)).ToList();
                _serLogger.Log($"subItemFreelist :{subItemFreelist}");

                if (bCheckBox)
                {
                    freeItemlist.AddRange(arrNewExtraSubItems);
                    _serLogger.Log($" bCheckBox is true and freeItemlist :{freeItemlist}");
                }
                else if (!bUpdateWithSelection)
                {
                    paidItemlist.AddRange(arrNewExtraSubItems);
                    _serLogger.Log($" bCheckBox is false and paidItemlist :{paidItemlist}");
                }

                var tmpFinalList = paidItemlist.Concat(freeItemlist).Concat(subItemFreelist).ToList();
                _serLogger.Log($" tmpFinalList :{tmpFinalList}");

                var arrNewCurrentSubitems = tmpFinalList.Where(x => !_initialItemParts.Contains(x) &&
                !_lstInitialSubItemFreeItemParts.Contains(x.Substring(1))
                /*ignore the subitems starting with '+' or '*' */
                ).ToArray();

                _serLogger.Log($"arrNewCurrentSubitems :{arrNewCurrentSubitems}");

                var finallist = bUpdateWithSelection
                            ?
                            _initialItemParts.Concat(arrNewCurrentSubitems).Concat(arrNewExtraSubItems).Distinct()
                            :
                            tmpFinalList.Distinct();
                _serLogger.Log($"finallist :{finallist.ToArray()}");

                lstBoxCurrentSubItems.Items.Clear();
                lstBoxCurrentSubItems.Items.AddRange(finallist.ToArray<object>());

                UpdateSelectionListBoxCurrentSubItems();
                _serLogger.Log("Done UpdateBoxCurrentSubitems done");
            }
            catch (Exception ex)
            {
                var strErrMsg = $"UpdateBoxCurrentSubitems Operation failed: {ex.Message} with InnerException: {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// Here we are done and return back to the calling form i.e. FormCustomerOrder
        /// ToDo :: logging needs to checked again as it does not print readable results.
        /// Too Complex.Refactoring needed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnOK_Click(object sender, EventArgs e)
        {
            try
            {
                var currentSubItems = lstBoxCurrentSubItems.Items.OfType<string>();
                var subItems = currentSubItems as string[] ?? currentSubItems.ToArray();
                _serLogger.Log($"subItems {subItems}");

                var itemName = _modifiedCurrentRow.Cells[PC.NAME].Value.ToString();
                //  var stringSeparators = Utilites.GetSeprators();

                Utilites.GetFirstandLastSeprator(itemName, out string firstSep, out string lastSep);

                if (!string.IsNullOrEmpty(lastSep))
                {
                    var res = itemName.Split(new[] { lastSep.Trim() }, StringSplitOptions.RemoveEmptyEntries);
                    var splitItemText = res.Select(p => p.Trim()).ToList();
                    splitItemText.RemoveAll(item => string.IsNullOrEmpty(item));
                    if (splitItemText.Count > 0)
                    {
                        var lastElement = splitItemText.Last();
                        subItems = subItems.OrderBy(d => d.Contains(lastElement)).ToArray();
                        var temp = lastSep.StartsWith(PC.COMMA, StringComparison.Ordinal) ? PC.COMMAWITHSPACE : lastSep;
                        if (subItems.Length > 0)
                            subItems[subItems.Length - 1] = temp + lastElement;
                    }
                }

                string finalItemName;
                if (subItems.Length > 0)
                {
                    var sortedList = subItems.OrderBy(o => lastSep != null && o.Contains(lastSep)).ToList();
                    if (sortedList.Count == 1)
                        firstSep = string.Empty;//PC.COMMAWITHSPACE;

                    finalItemName = lblModifyingItem.Text + firstSep
                         + string.Join(PC.COMMAWITHSPACE, sortedList.Take(sortedList.Count - 1)).ToLower()
                        + sortedList.Last().ToLower();
                }
                else
                {
                    finalItemName = lblModifyingItem.Text;
                }

                _serLogger.Log($"finalValue {finalItemName}");

                _modifiedCurrentRow.Cells[PC.NAME].Value = finalItemName;
                _serLogger.Log($"Item Name { _modifiedCurrentRow.Cells[PC.NAME].Value}");

                _modifiedCurrentRow.DataGridView.NotifyCurrentCellDirty(true);
                _modifiedCurrentRow.Cells[PC.PREIS].Value = Utilites.ConvertStringToDecimal(txtPrice.Text);
                _serLogger.Log($"Item Preis {_modifiedCurrentRow.Cells[PC.PREIS].Value}");
                _modifiedCurrentRow.DataGridView.NotifyCurrentCellDirty(false);
            }
            catch (Exception ex)
            {
                var strErrMsg = $"Ok Operation failed: {ex.Message} with InnerException: {ex.InnerException}";
                _serLogger.Log(strErrMsg);
                throw;
            }
        }

        /// <summary>
        /// Display the count of the currently added paid items.
        /// </summary>
        private void DisplayAddedSubItemsCount()
        {
            var count = CurrentSubItemsCount();
            _serLogger.Log($"CurrentSubItemsCount {CurrentSubItemsCount()}");

            lblAddedSubItems.Text = count > _initialItemPartsCount
                                 ? $"NewSubItems:{count - _initialItemPartsCount}"
                                 : "NewSubItems:0";
            _serLogger.Log($"lblAddedSubItems.Text {lblAddedSubItems.Text}");
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <returns></returns>
        internal int CurrentSubItemsCount()
        {
            return lstBoxCurrentSubItems.Items.OfType<string>().Count(x => !x.StartsWith(PC.ASTERIX, StringComparison.Ordinal) && !x.StartsWith(PC.PLUS, StringComparison.Ordinal));
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        private void UpdateSelectionListBoxCurrentSubItems()
        {
            var currentSubItems = lstBoxCurrentSubItems.Items.OfType<string>();

            var paidItemlist = currentSubItems.Where(x => !x.StartsWith(PC.ASTERIX, StringComparison.Ordinal) && !x.StartsWith(PC.PLUS, StringComparison.Ordinal)).ToList();
            var lstNewCurrentSubitems = paidItemlist.Where(x => !_initialItemParts.Contains(x));

            lstNewCurrentSubitems.Where(x => x?.StartsWith(PC.ASTERIX, StringComparison.Ordinal) == false
              && !x.StartsWith(PC.PLUS, StringComparison.Ordinal) && !_initialItemParts.Contains(x)).ToList()
           .ForEach(i => lstBoxCurrentSubItems.SelectedItem = i);
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstBoxOptionPaidSubItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAddSubItem.Enabled = lstBoxOptionPaidSubItems.SelectedItems.Count > 0;
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstBoxCurrentSubItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemoveSubItem.Enabled = lstBoxCurrentSubItems.SelectedItems.Count > 0;
        }

        private async Task ExecuteCommonCode()
        {
            DisplayAddedSubItemsCount();
            var itemPrice = await _itPriceCalculation.CalculateItemPriceAsync(_modifiedCurrentRow, CurrentSubItemsCount()).ConfigureAwait(false);
            if (txtPrice.InvokeRequired)
            {
                txtPrice.Invoke(new Action(() => txtPrice.Text = itemPrice.ToString(PC.CURRENCY)));
            }
            else
            {
                txtPrice.Text = itemPrice.ToString(PC.CURRENCY);
            }
        }
    }
}
