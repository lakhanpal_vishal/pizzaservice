﻿using System.Collections.Specialized;
using System.Configuration;

namespace PizzaService
{
    public class Configuration : IConfiguration
    {
        #region Configuration Members
        private NameValueCollection _collExtrasConfigSettings;
        #endregion Configuration Members

        #region Configuration Methods
        /// <summary>
        /// Read settings from the Config File
        /// </summary>
        /// <param name="strInput"></param>
        /// <returns></returns>
        public decimal GetExtraConfigSetting(string strInput)
        {
            if (_collExtrasConfigSettings == null)
                _collExtrasConfigSettings = (NameValueCollection)ConfigurationManager.GetSection("ExtrasPriceConfig");
            if (_collExtrasConfigSettings != null)
                return Utilites.ConvertStringToDecimal(_collExtrasConfigSettings[strInput]);
            return 0.0m;
        }
        #endregion Configuration Methods
    }
}
