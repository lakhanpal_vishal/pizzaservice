﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Printing;
using System.Data.SQLite;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
// add kommentar hier
namespace DbBase
{
    public class Artist
    {
        public Artist()
        {
            Albums = new List<Album>();
        }

        public long ArtistId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Album> Albums { get; set; }
    }

    public class Album
    {
        public long AlbumId { get; set; }
        public string Title { get; set; }

        public long ArtistId { get; set; }
        public virtual Artist Artist { get; set; }
    }

    class MyDbContext : DbContext
    {
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Chinook Database does not pluralize table names
            modelBuilder.Conventions
                .Remove<PluralizingTableNameConvention>();
        }
    }

    public class Vish
    {
        public static void callme()
{
            using (var context = new MyDbContext())
            {
                var artists = from a in context.Artists
                              where a.Name.StartsWith("Z")
                              orderby a.Name
                              select a;


                foreach (var artist in artists)
                {
                    Console.WriteLine(artist.Name);
                }

                var result = context.Artists.SingleOrDefault(b => b.ArtistId == 155);
                if (result != null)
                {
                //    result.Name = "Zeca1234 Pagodinho";
                //    context.SaveChanges();
                //    context.Artists.Remove(result);
                }
            }

            using (var context = new MyDbContext())
            {
                context.Artists.Add(
                    new Artist
                    {
                        Name = "Anberlin",
                        Albums =
            {
                new Album { Title = "Cities" },
                new Album { Title = "New Surrender" }
            }
                    });
                context.SaveChanges();
            }
        }
    }



//    [Table(Name = "Company")]
//    class Company
//    {
//        [Column(Name = "Id")]
//        public int Id { get; set; }

//        [Column(Name = "seats")]
//        public string Seats { get; set; }
//    }
    
    
//    class DataBase
//    {
//        private static SQLiteConnection sql_con;
//        private SQLiteCommand sql_cmd;
//        private SQLiteDataAdapter DB;
//        private DataSet DS = new DataSet();
//        private DataTable DT = new DataTable();

//        private static SQLiteConnection SetConnection()
//        {
//            if (sql_con == null)
//                sql_con = new System.Data.SQLite.SQLiteConnection("Data Source=DemoT.db;Version=3;New=False;Compress=True;");
//            var context = new DataContext(sql_con);

//            var companies = context.GetTable<Company>();
            
//            foreach (Company comp in companies)
//            {
//                //Console.WriteLine("Company: {0} {1}",
//                  //  company.Id, company.Seats);
//                comp.Seats = "ww"; 
                
//            }

//            var updateItemQuery = (from c in context.GetTable<Company>()

//                                   where c.Id == 1

//                                   select c);

//            Company ordd = updateItemQuery.FirstOrDefault();

//            if (ordd != null)
//            {

//                ordd.Seats = "xx";

//                sql_con.BeginTransaction();

//                sql_con.Update += new SQLiteUpdateEventHandler(DataBase.TextChanged);

//                //sql_con.Commit+=new SQLiteCommitHandler()

//            }

//            sql_con.Dispose();

//            sql_con.Close();


//            context.SubmitChanges();
            
//                return sql_con;
//        }

//        private static void TextChanged(object sender, EventArgs e)
//        {
            
//        }


//        public void ExecuteQuery(string txtQuery)
//        {
//            SetConnection();
//            try
//            {
//                sql_con.Open();
//                sql_cmd = sql_con.CreateCommand();
//                sql_cmd.CommandText = txtQuery;
//                sql_cmd.ExecuteNonQuery();
//                sql_con.Close();
//            }
//            catch (Exception ex)
//            {
//                MessageBox.Show("Operation failed: " + ex.Message.ToString(),
//                                Application.ProductName +
//                                " - Error", MessageBoxButtons.OK,
//                                MessageBoxIcon.Error);
//            }
//        }

//        public void LoadData(ref string CommandText,ref string strvorname,ref string strnachname,
//                             ref string strstrasse,ref string strort,ref int nplz )
//        {
//            SetConnection();
//            sql_con.Open();
//            sql_cmd = sql_con.CreateCommand();
//            DB = new SQLiteDataAdapter(CommandText, sql_con);
//            DS.Reset();
//            DB.Fill(DS);
//           // string str = "Customer";
//            DT = DS.Tables[0];
//                //DT.BeginLoadData
//                //LoadDataRow
// //DataTable..::.CreateDataReader
//            DataRow[] newRow = DT.Select();
//            if (newRow.Length == 0)
//            {
//                CommandText = string.Empty;
//                sql_con.Close();
//                return;
//            }
//            else
//            {
//                CommandText = newRow[0]["telefon"].ToString();
//                strvorname = newRow[0]["Name1"].ToString();
//                strnachname = newRow[0]["Name2"].ToString();
//                strstrasse = newRow[0]["street"].ToString();
//                strort = newRow[0]["city"].ToString();
//                nplz = System.Convert.ToInt32(newRow[0]["pin"].ToString(), 10); 
//            }
//            sql_con.Close();
//        }
//    }
}
