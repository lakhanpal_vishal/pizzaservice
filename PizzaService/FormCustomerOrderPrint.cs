﻿using Microsoft.Reporting.WinForms;
using PizzaServiceDB.DbBase;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;
using System.Windows.Forms;

namespace PizzaService
{
    public partial class FormCustomerOrderPrint : Form
    {
        private List<CustomerBills> _listCustomerBills;
        private int _currentPageIndex;
        private IList<Stream> _streams;
        private Customer _customer;
        private readonly IPizzaServiceLogger _serLogger;

        public FormCustomerOrderPrint(IPizzaServiceLogger serLogger)
        {
            _serLogger = serLogger;
            InitializeComponent();
        }

        public void PrepareToPrint(List<CustomerBills> listCustomerBills, Customer customer, bool bPrintCustomerBill = true)
        {
            _listCustomerBills = listCustomerBills;
            _customer = customer;
            _serLogger.Log($"Preparing for the Printing Bill: {_listCustomerBills.Select(x => x.CustomerBillItemId).ToList()} for Customer with Id : {_customer.Id}");
            PrepareBillForPrinting(bPrintCustomerBill);
            _serLogger.Log($"Preparing for the Printing Bill done: {_listCustomerBills.Select(x => x.CustomerBillItemId).ToList()} for Customer with Id : {_customer.Id}");
        }

        /// <summary>
        ///  Routine to provide to the report renderer, in order to
        ///  save an image for each page of the report.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="fileNameExtension"></param>
        /// <param name="encoding"></param>
        /// <param name="mimeType"></param>
        /// <param name="willSeek"></param>
        /// <returns></returns>
        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            var stream = new MemoryStream();
            _streams.Add(stream);
            return stream;
        }

        /// <summary>
        /// Export the given report as an EMF (Enhanced Metafile) file.
        /// </summary>
        /// <param name="report"></param>
        public void Export(LocalReport report)
        {
            const string deviceInfo =
              @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>8.5in</PageWidth>
                <PageHeight>11in</PageHeight>
                <MarginTop>0.25in</MarginTop>
                <MarginLeft>0.25in</MarginLeft>
                <MarginRight>0.25in</MarginRight>
                <MarginBottom>0.25in</MarginBottom>
            </DeviceInfo>";
            _streams = new List<Stream>();
            // ReSharper disable once UnusedVariable
            report.Render("Image", deviceInfo, CreateStream, out Warning[] warnings);
            foreach (Stream stream in _streams)
                stream.Position = 0;
        }

        /// <summary>
        /// Handler for PrintPageEvents
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            var pageImage = new Metafile(_streams[_currentPageIndex]);

            // Adjust rectangular area with printer margins.
            var adjustedRect = new Rectangle(
                ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                ev.PageBounds.Width,
                ev.PageBounds.Height);

            // Draw a white background for the report
            ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

            // Draw the report content
            ev.Graphics.DrawImage(pageImage, adjustedRect);

            // Prepare for the next page. Make sure we haven't hit the end.
            _currentPageIndex++;
            ev.HasMorePages = _currentPageIndex < _streams.Count;
        }

        /// <summary>
        /// Finally print the customer Bill now.
        /// </summary>
        public void Print()
        {
            try
            {
                if (_streams == null || _streams.Count == 0)
                    throw new ArgumentException($"Error: no stream to print: {nameof(_streams)}");

                using (var printDoc = new PrintDocument())
                {
                    //  printDoc.PrintController = new System.Drawing.Printing.StandardPrintController();
                    if (!printDoc.PrinterSettings.IsValid)
                    {
                        throw new InvalidPrinterException(printDoc.PrinterSettings);
                    }
                    printDoc.PrinterSettings.PrinterName = "Microsoft XPS Document Writer";
                    var printDirectory = AppDomain.CurrentDomain.BaseDirectory + "Print";
                    if (!Directory.Exists(printDirectory))
                        Directory.CreateDirectory(printDirectory);

                    printDoc.PrinterSettings.PrintFileName = printDirectory + @"\" + _customer.Id +
                       "__"
                       + _listCustomerBills[0].CustomerBillPrintTime.Day + "."
                       + _listCustomerBills[0].CustomerBillPrintTime.Month + "."
                       + _listCustomerBills[0].CustomerBillPrintTime.Year +
                       "__"
                       + _listCustomerBills[0].CustomerBillPrintTime.Hour + "."
                       + _listCustomerBills[0].CustomerBillPrintTime.Minute + "."
                       + _listCustomerBills[0].CustomerBillPrintTime.Second
                       + ".xps";
                    printDoc.PrinterSettings.PrintToFile = true;
                    printDoc.PrintPage += PrintPage;
                    _currentPageIndex = 0;
                    printDoc.Print();
                }
            }
            catch (InvalidPrinterException ex)
            {
                _serLogger.Log($" printing the Customer Bill failed : {ex.Message} with InnerException: {ex.InnerException}");
            }
            catch (Exception ex)
            {
                _serLogger.Log($" printing the Customer Bill failed due to unknown reason: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
        }

        /// <summary>
        /// Here we prepare the parameters for the local report
        /// </summary>
        internal void PrepareBillForPrinting(bool bPrintCustomerBill)
        {
            _serLogger.Log("Start PrepareBillForPrinting");
            reportViewerPrint.LocalReport.DataSources.Clear(); //clear report
            var assemblyName = Assembly.GetExecutingAssembly().GetName();
            var pkBlob = assemblyName.GetPublicKeyToken();
            var sName = new StrongName(new StrongNamePublicKeyBlob(pkBlob), assemblyName.Name, assemblyName.Version);
            //reportViewerPrint.LocalReport.AddFullTrustModuleInSandboxAppDomain(sName);

            var permissions = new PermissionSet(PermissionState.None);
            permissions.AddPermission(new FileIOPermission(PermissionState.Unrestricted));
            permissions.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
           // reportViewerPrint.LocalReport.SetBasePermissionsForSandboxAppDomain(permissions);

            var dataset = new ReportDataSource("DataSet1", _listCustomerBills); // set the datasource
            reportViewerPrint.LocalReport.DataSources.Add(dataset);
             _serLogger.Log("Dataset added successfully");

            var customerDetails = _customer.FirstName + " " + _customer.LastName
                                + Environment.NewLine + _customer.Address
                                + Environment.NewLine + _customer.City
                                + Environment.NewLine + _customer.Postalcode
                                + Environment.NewLine + _customer.Phone;

            var rpCustomerdetails = new ReportParameter("ReportParameterCusDetails", customerDetails);
            _serLogger.Log($"rpCustomerdetails for Customer LastName :{_customer.LastName}");

            var rpCustomerBillNumber = new ReportParameter("ReportParameterCustomerBillNumber",
                $"RechnungNr: {_listCustomerBills[0].CustomerBillNumber}");
            _serLogger.Log($"CustomerBillNumber :{_listCustomerBills[0].CustomerBillNumber}");

            var rpCustomerId = new ReportParameter("ReportParameterCustomerId",
                $"KundeNr.: {_listCustomerBills[0].CustomerRefId}");
            _serLogger.Log($"CustomerId :{_listCustomerBills[0].CustomerRefId}");

            var rpCustomerOrderTime = new ReportParameter("ReportParameterCustomerOrderTime",
                $"BestellungsZeit: {_listCustomerBills[0].CustomerBillPrintTime.ToString(CultureInfo.CurrentCulture)}");

            _serLogger.Log($"CustomerOrderTime :{_listCustomerBills[0].CustomerBillPrintTime}");
            reportViewerPrint.LocalReport.SetParameters(new[] { rpCustomerdetails, rpCustomerBillNumber, rpCustomerId, rpCustomerOrderTime });
            _serLogger.Log("SetParameters done");
            if (bPrintCustomerBill)
            {
                _serLogger.Log("setting Print mode");
                Export(reportViewerPrint.LocalReport);
                _serLogger.Log("Print mode done");
            }
            else
            {
                reportViewerPrint.RefreshReport();
                _serLogger.Log("Printpreview mode done");
            }
        }
    }
}
