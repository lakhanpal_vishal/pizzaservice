﻿namespace PizzaService
{
    public interface IPizzaServiceLogger
    {
        void Log(LogEntry entry);
    }
    public enum LoggingEventType { /*Debug*/ Information, /*Warning*/ Error /*Fatal*/ };
}
