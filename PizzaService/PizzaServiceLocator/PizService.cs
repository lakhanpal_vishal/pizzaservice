﻿using PizzaServiceDB.PizzaServiceRepository;
using PizzaServiceDB.PizzaServiceUnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceLocator
{
    public class PizService<T> : IPizService<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPizzaServiceRepository<T> _pizzaRepository;

        public PizService(IUnitOfWork unitOfWork, IPizzaServiceRepository<T> pizzaRepository)
        {
            _unitOfWork = unitOfWork;
            _pizzaRepository = pizzaRepository;
        }

        public async Task UpdateAsync(T entity)
        {
            _pizzaRepository.Update(entity);
            await _unitOfWork.Commit().ConfigureAwait(false);
        }

        public async Task SaveAsync(T entity)
        {
            _pizzaRepository.Insert(entity);
            await _unitOfWork.Commit().ConfigureAwait(false);
        }

        public async Task SaveAllAsync(List<T> entities)
        {
            _pizzaRepository.InsertRange(entities);
            await _unitOfWork.Commit().ConfigureAwait(false);
        }

        /// <summary>
        /// Searches an Entity based on the Predicate.
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="navProperty"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty)
        {
            return await _pizzaRepository.SearchForAsync(predicate, navProperty).ConfigureAwait(false);
        }

        /// <summary>
        /// Returns all the Entities from the current DB Table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll()
        {
            return _pizzaRepository.GetAll();
        }

        /// <summary>
        /// Returns an Entity based upon the id of the Table.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T</returns>
        public T GetById(int id)
        {
            return _pizzaRepository.GetById(id);
        }

        public IEnumerable<T> ExecuteRawSqlQuery(T entity, string sql)
        {
            return _unitOfWork.ExecuteRawSqlQuery(entity, sql);
        }
    }
}
