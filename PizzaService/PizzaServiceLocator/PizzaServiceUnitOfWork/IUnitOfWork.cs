﻿using PizzaService.DbBase;
using PizzaService.PizzaServiceRepository;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceUnitOfWork
{
    public interface IUnitOfWork
    {
        Task Commit();
        IEnumerable<T> ExecuteRawSqlQuery<T>(T entity, string sql) where T : class;
    }
}
