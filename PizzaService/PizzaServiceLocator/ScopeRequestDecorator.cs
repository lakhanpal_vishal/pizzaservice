﻿using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceLocator
{
    public sealed class ScopeRequestDecorator<T> : IPizService<T> where T : class
    {
        private readonly Container _container;
        private readonly Func<IPizService<T>> _decoratee;

        public ScopeRequestDecorator(Container container, Func<IPizService<T>> decoratee)
        {
            _container = container;
            _decoratee = decoratee;
        }

        public IQueryable<T> GetAll()
        {
            return _decoratee.Invoke().GetAll();
        }

        public T GetById(int id)
        {
            return _decoratee.Invoke().GetById(id);
        }

        public async Task SaveAllAsync(List<T> entities)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                await _decoratee.Invoke().SaveAllAsync(entities).ConfigureAwait(false);
            }
        }

        public async Task SaveAsync(T entity)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                await _decoratee.Invoke().SaveAsync(entity).ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                return await _decoratee.Invoke().SearchForAsync(predicate, navProperty).ConfigureAwait(false);
            }
        }

        public async Task UpdateAsync(T entity)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                await _decoratee.Invoke().UpdateAsync(entity).ConfigureAwait(false);
            }
        }

        public IEnumerable<T> ExecuteRawSqlQuery(T entity, string sql)
        {
            using (AsyncScopedLifestyle.BeginScope(_container))
            {
                return _decoratee.Invoke().ExecuteRawSqlQuery(entity, sql);
            }
        }
    }
}
