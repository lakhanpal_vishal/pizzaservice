﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceRepository
{
    public interface IPizzaServiceRepository<T> where T : class
    {
        void InsertRangeAsync(List<T> entity);
        void InsertAsync(T entity);
        void UpdateAsync(T entity);
        Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty = null);
        IQueryable<T> GetAll();
        T GetById(int id);
    }
}
