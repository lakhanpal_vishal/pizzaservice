﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceLocator
{
    public interface IPizService<T> where T : class
    {
        Task UpdateAsync(T entity);
        Task SaveAllAsync(List<T> items);
        Task SaveAsync(T members);
        Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty = null);
        IQueryable<T> GetAll();
        T GetById(int id);
        IEnumerable<T> ExecuteRawSqlQuery(T entity, string sql);
    }
}
