﻿using PizzaServiceDB.DbBase;
using System.Threading.Tasks;
using CustomerOrderGridRow = System.Windows.Forms.DataGridViewRow;
namespace PizzaService.BusinessLogic
{
    public interface IItemPriceCalculation
    {
        void SetCurrentCustomer(Customer currCustomer);
        Task<decimal> CalculateItemPriceAsync(CustomerOrderGridRow currentRow, int currentSubItemsCount = -1);
    }
}
