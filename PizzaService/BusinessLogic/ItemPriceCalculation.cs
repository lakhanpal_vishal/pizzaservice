﻿using PizzaService.PizzaServiceLocator;
using PizzaServiceDB.DbBase;
using System;
using System.Linq;
using System.Threading.Tasks;
using CustomerOrderGridRow = System.Windows.Forms.DataGridViewRow;
using PC = PizzaService.PizzaServiceConstants;
namespace PizzaService.BusinessLogic
{
    public sealed class ItemPriceCalculation : IItemPriceCalculation
    {
        #region ItemPriceCalculation Members
        public Customer _currCustomer;
        private readonly IPizService<Items> _serItems;
        private readonly IPizzaServiceLogger _serLogger;
        private readonly IConfiguration _configOption;
        #endregion ItemPriceCalculation Members

        #region ItemPriceCalculation Methods
        public ItemPriceCalculation(IPizService<Items> serItems, IPizzaServiceLogger serLogger, IConfiguration configOption)
        {
            _serItems = serItems;
            _serLogger = serLogger;
            _configOption = configOption;
        }
        /// <summary>
        /// self explanatory
        /// </summary>
        /// <param name="currCustomer"></param>
        public void SetCurrentCustomer(Customer currCustomer)
        {
            _currCustomer = currCustomer;
          _serLogger.Log($"Setting the current customer: {_currCustomer.Id}");
        }

        /// <summary>
        /// Here we get the Price of the Item depending upon the size of the
        /// Customer Item, Default will be Pricesingle and for others it will be
        /// PriceJumbo, etc...
        /// </summary>
        /// <param name="item"></param>
        /// <param name="strItemSize"></param>
        /// <returns></returns>
        private string GetSizeDependentPriceForCustomerItem(Items item, string strItemSize)
        {
            if (!strItemSize.Contains(PC.PRICE))
                strItemSize = $"Price{strItemSize}";
            _serLogger.Log($"Item has Id: {item.Id}: with size: {strItemSize}");

            // ReSharper disable once PossibleNullReferenceException. todo
            var itemValue = (decimal)item.GetType().GetProperty(strItemSize).GetValue(item);
            _serLogger.Log($"Item has Id: {item.Id}: with Price: {itemValue}");

            return Utilites.ConvertDecimalToString(itemValue);
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <param name="currentRow"></param>
        /// <param name="currentSubItemsCount"></param>
        /// <returns></returns>
        public async Task<decimal> CalculateItemPriceAsync(CustomerOrderGridRow currentRow, int currentSubItemsCount = -1)
        {
            try
            {
                if (currentRow == null)
                {
                    throw new ArgumentNullException(nameof(currentRow), "CurrentRow is null.");
                }

                int.TryParse(currentRow.Cells[PC.NUMMER].Value.ToString(), out int cellVal);
                var items = await _serItems.SearchForAsync(x => x.ItemId == cellVal).ConfigureAwait(false);
                var enumerable = items as Items[] ?? items.ToArray();
                if (enumerable.Length == 0)
                {
                    _serLogger.Log("item is null");
                    throw new ArgumentException(nameof(items), $"Type {typeof(Items)} does not exit.");
                }
                var item = enumerable[0];
                _serLogger.Log($"item is found {item.ItemId}");

                var stringSeparators = Utilites.GetSeprators();
                var splitItemText = item.ItemName.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries)
                                                       .Select(i => i.Trim()).ToArray();
                var nInitialSubItemCount = splitItemText.Length - 1;

                //Cells[1] = Item name
                int.TryParse(currentRow.Cells[PC.ANZAHL].Value.ToString(), out int nInitialNumberOfItems);
                _serLogger.Log($"InitialNumberOfItems is found {nInitialNumberOfItems}");

                if (currentSubItemsCount == -1)
                {
                    var strSubItemText = currentRow.Cells[PC.NAME].Value.ToString();
                    _serLogger.Log($"strSubItemText: {strSubItemText}");
                    var res = strSubItemText.Split(Utilites.GetSeprators(), StringSplitOptions.RemoveEmptyEntries);
                    var parts = res.Select(p => p.Trim()).ToList();
                    currentSubItemsCount = parts.Skip(1).Count(x => x?.StartsWith(PC.ASTERIX, StringComparison.Ordinal) == false && !x.StartsWith(PC.PLUS, StringComparison.Ordinal));
                    _serLogger.Log($"currentSubItemsCount: {currentSubItemsCount}");
                }

                int nChangedSubItemCount = 0;
                if (currentSubItemsCount > nInitialSubItemCount)
                    nChangedSubItemCount = currentSubItemsCount - nInitialSubItemCount;

                _serLogger.Log($"nChangedSubItemCount: {nChangedSubItemCount}");

                var strItemInitialSize = currentRow.Cells[PC.GRÖSSE].Value.ToString();
                strItemInitialSize = $"Price{strItemInitialSize}";
                _serLogger.Log($"strItemInitialSize: {strItemInitialSize}");

                var strItemInitialStartPrice = GetSizeDependentPriceForCustomerItem(item, strItemInitialSize);
                _serLogger.Log($"strItemInitialStartPrice: {strItemInitialStartPrice}");

                var dItemStartPrice = Utilites.ConvertStringToDecimal(strItemInitialStartPrice);
                _serLogger.Log($"ItemStartPrice: {dItemStartPrice}");

                var dPriceNewlyAddedSubItem = nChangedSubItemCount * _configOption.GetExtraConfigSetting(strItemInitialSize);
                dPriceNewlyAddedSubItem = nInitialNumberOfItems * dPriceNewlyAddedSubItem;
                _serLogger.Log($"PriceNewlyAddedSubItem: {dPriceNewlyAddedSubItem}");

                var dFinalPrice = (nInitialNumberOfItems * dItemStartPrice) + dPriceNewlyAddedSubItem;
                _serLogger.Log($"FinalPrice: {dFinalPrice}");

                // We check here for the Abholer
                if (_currCustomer.FirstName?.Contains(PC.ABHOLER) == true)
                {
                    _serLogger.Log("Abholer is true");
                    dFinalPrice = (100 - _configOption.GetExtraConfigSetting(PC.ABHOLER)) / 100 * dFinalPrice;
                    _serLogger.Log($"FinalPrice: {dFinalPrice}");
                }
                return Math.Round(dFinalPrice, 2);
            }
            catch (Exception ex)
            {
                _serLogger.Log($"Exception occurred: {ex.Message} with InnerException: {ex.InnerException}");
                throw;
            }
        }
    }
    #endregion ItemPriceCalculation Methods
}
