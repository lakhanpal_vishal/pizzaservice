﻿
namespace PizzaService
{
    public interface IConfiguration
    {
        decimal GetExtraConfigSetting(string strInput);
    }
}
