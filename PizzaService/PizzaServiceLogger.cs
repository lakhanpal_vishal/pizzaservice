﻿using log4net;
using System;
//Too complex, needs to be worked
namespace PizzaService
{
    public static class LoggerExtensions
    {
        public static void Log(this IPizzaServiceLogger logger, string message)
        {
            logger.Log(new LogEntry(LoggingEventType.Information, message, null)
                      );
        }

        // ReSharper disable once UnusedMember.Global
        public static void Log(this IPizzaServiceLogger logger, Exception exception)
        {
            logger.Log(new LogEntry(LoggingEventType.Error, exception.Message, exception)
                      );
        }
        //Workaround for mocking the extension methods...
        static LoggerExtensions()
        {
            DoPizzaServiceLoggerMock = log => new Log4NetAdapter(log);
        }

        public static Func<ILog, IPizzaServiceLogger> DoPizzaServiceLoggerMock { get; set; }
    }

    public class LogEntry
    {
        public readonly LoggingEventType _severity;
        public readonly string _message;
        public readonly Exception _exception;

        public LogEntry(LoggingEventType severity, string message, Exception exception = null)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            if (message?.Length == 0)
                throw new ArgumentException(nameof(message), nameof(exception));

            _severity = severity;
            _message = message;
            _exception = exception;
        }
    }

    public class Log4NetAdapter : IPizzaServiceLogger
    {
        private readonly ILog _adaptee;
        public Log4NetAdapter(ILog adaptee) => _adaptee = adaptee;

        public void Log(LogEntry entry)
        {
            switch (entry._severity)
            {
                //case LoggingEventType.Debug:
                //    _adaptee.Debug(entry._message, entry._exception);
                //    break;
                case LoggingEventType.Information:
                    _adaptee.Info(entry._message, entry._exception);
                    break;
                //case LoggingEventType.Warning:
                //    _adaptee.Warn(entry._message, entry._exception);
                //    break;
                case LoggingEventType.Error:
                    _adaptee.Error(entry._message, entry._exception);
                    break;
                default:
                    _adaptee.Fatal(entry._message, entry._exception);
                    break;
            }
        }
    }
}

