﻿namespace PizzaService
{
    partial class FormCustomerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInsert = new System.Windows.Forms.Button();
            this.lblTelefon = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.lblVorname = new System.Windows.Forms.Label();
            this.txtVorname = new System.Windows.Forms.TextBox();
            this.lblNachname = new System.Windows.Forms.Label();
            this.txtNachName = new System.Windows.Forms.TextBox();
            this.lblStrasse = new System.Windows.Forms.Label();
            this.txtStrasse = new System.Windows.Forms.TextBox();
            this.lblOrt = new System.Windows.Forms.Label();
            this.txtOrt = new System.Windows.Forms.TextBox();
            this.lblPlz = new System.Windows.Forms.Label();
            this.txtPlz = new System.Windows.Forms.TextBox();
            this.btnModify = new System.Windows.Forms.Button();
            this.chkSelfPicker = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnInsert
            // 
            this.btnInsert.BackColor = System.Drawing.Color.YellowGreen;
            this.btnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnInsert.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.ForeColor = System.Drawing.Color.LightSlateGray;
            this.btnInsert.Location = new System.Drawing.Point(232, 403);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(156, 38);
            this.btnInsert.TabIndex = 0;
            this.btnInsert.Text = "Hizufugen";
            this.btnInsert.UseVisualStyleBackColor = false;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_ClickAsync);
            // 
            // lblTelefon
            // 
            this.lblTelefon.AutoSize = true;
            this.lblTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefon.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblTelefon.Location = new System.Drawing.Point(61, 109);
            this.lblTelefon.Name = "lblTelefon";
            this.lblTelefon.Size = new System.Drawing.Size(228, 29);
            this.lblTelefon.TabIndex = 1;
            this.lblTelefon.Text = "KundenNr/Telefon";
            // 
            // txtTelefon
            // 
            this.txtTelefon.BackColor = System.Drawing.Color.Linen;
            this.txtTelefon.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefon.ForeColor = System.Drawing.Color.Brown;
            this.txtTelefon.Location = new System.Drawing.Point(325, 107);
            this.txtTelefon.MaxLength = 20;
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(233, 31);
            this.txtTelefon.TabIndex = 2;
            // 
            // lblVorname
            // 
            this.lblVorname.AutoSize = true;
            this.lblVorname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVorname.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblVorname.Location = new System.Drawing.Point(139, 153);
            this.lblVorname.Name = "lblVorname";
            this.lblVorname.Size = new System.Drawing.Size(117, 29);
            this.lblVorname.TabIndex = 3;
            this.lblVorname.Text = "Vorname";
            // 
            // txtVorname
            // 
            this.txtVorname.BackColor = System.Drawing.Color.Linen;
            this.txtVorname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVorname.ForeColor = System.Drawing.Color.Brown;
            this.txtVorname.Location = new System.Drawing.Point(325, 153);
            this.txtVorname.Name = "txtVorname";
            this.txtVorname.Size = new System.Drawing.Size(233, 31);
            this.txtVorname.TabIndex = 4;
            // 
            // lblNachname
            // 
            this.lblNachname.AutoSize = true;
            this.lblNachname.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNachname.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblNachname.Location = new System.Drawing.Point(139, 199);
            this.lblNachname.Name = "lblNachname";
            this.lblNachname.Size = new System.Drawing.Size(137, 29);
            this.lblNachname.TabIndex = 10;
            this.lblNachname.Text = "Nachname";
            // 
            // txtNachName
            // 
            this.txtNachName.BackColor = System.Drawing.Color.Linen;
            this.txtNachName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNachName.ForeColor = System.Drawing.Color.Brown;
            this.txtNachName.Location = new System.Drawing.Point(325, 199);
            this.txtNachName.Name = "txtNachName";
            this.txtNachName.Size = new System.Drawing.Size(233, 31);
            this.txtNachName.TabIndex = 11;
            // 
            // lblStrasse
            // 
            this.lblStrasse.AutoSize = true;
            this.lblStrasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStrasse.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblStrasse.Location = new System.Drawing.Point(139, 243);
            this.lblStrasse.Name = "lblStrasse";
            this.lblStrasse.Size = new System.Drawing.Size(101, 29);
            this.lblStrasse.TabIndex = 12;
            this.lblStrasse.Text = "Strasse";
            // 
            // txtStrasse
            // 
            this.txtStrasse.BackColor = System.Drawing.Color.Linen;
            this.txtStrasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStrasse.ForeColor = System.Drawing.Color.Brown;
            this.txtStrasse.Location = new System.Drawing.Point(325, 245);
            this.txtStrasse.Name = "txtStrasse";
            this.txtStrasse.Size = new System.Drawing.Size(233, 31);
            this.txtStrasse.TabIndex = 13;
            // 
            // lblOrt
            // 
            this.lblOrt.AutoSize = true;
            this.lblOrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrt.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblOrt.Location = new System.Drawing.Point(139, 292);
            this.lblOrt.Name = "lblOrt";
            this.lblOrt.Size = new System.Drawing.Size(49, 29);
            this.lblOrt.TabIndex = 14;
            this.lblOrt.Text = "Ort";
            // 
            // txtOrt
            // 
            this.txtOrt.BackColor = System.Drawing.Color.Linen;
            this.txtOrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrt.ForeColor = System.Drawing.Color.Brown;
            this.txtOrt.Location = new System.Drawing.Point(325, 291);
            this.txtOrt.Name = "txtOrt";
            this.txtOrt.Size = new System.Drawing.Size(233, 31);
            this.txtOrt.TabIndex = 15;
            // 
            // lblPlz
            // 
            this.lblPlz.AutoSize = true;
            this.lblPlz.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlz.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.lblPlz.Location = new System.Drawing.Point(139, 340);
            this.lblPlz.Name = "lblPlz";
            this.lblPlz.Size = new System.Drawing.Size(49, 29);
            this.lblPlz.TabIndex = 16;
            this.lblPlz.Text = "Plz";
            // 
            // txtPlz
            // 
            this.txtPlz.BackColor = System.Drawing.Color.Linen;
            this.txtPlz.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPlz.ForeColor = System.Drawing.Color.Brown;
            this.txtPlz.Location = new System.Drawing.Point(325, 337);
            this.txtPlz.MaxLength = 5;
            this.txtPlz.Name = "txtPlz";
            this.txtPlz.Size = new System.Drawing.Size(233, 31);
            this.txtPlz.TabIndex = 17;
            this.txtPlz.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPlz_KeyDown);
            this.txtPlz.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPlz_KeyPress);
            // 
            // btnModify
            // 
            this.btnModify.BackColor = System.Drawing.Color.YellowGreen;
            this.btnModify.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModify.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModify.ForeColor = System.Drawing.Color.LightSlateGray;
            this.btnModify.Location = new System.Drawing.Point(232, 411);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(156, 38);
            this.btnModify.TabIndex = 18;
            this.btnModify.Text = "Anpassen";
            this.btnModify.UseVisualStyleBackColor = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_ClickAsync);
            // 
            // chkSelfPicker
            // 
            this.chkSelfPicker.AutoSize = true;
            this.chkSelfPicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSelfPicker.ForeColor = System.Drawing.Color.SlateBlue;
            this.chkSelfPicker.Location = new System.Drawing.Point(34, 382);
            this.chkSelfPicker.Name = "chkSelfPicker";
            this.chkSelfPicker.Size = new System.Drawing.Size(112, 29);
            this.chkSelfPicker.TabIndex = 20;
            this.chkSelfPicker.Text = "Abholer";
            this.chkSelfPicker.UseVisualStyleBackColor = true;
            this.chkSelfPicker.Click += new System.EventHandler(this.chkSelfPicker_ClickAsync);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.YellowGreen;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.LightSlateGray;
            this.btnCancel.Location = new System.Drawing.Point(409, 411);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(156, 38);
            this.btnCancel.TabIndex = 21;
            this.btnCancel.Text = "Schließen";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormCustomerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.ClientSize = new System.Drawing.Size(696, 477);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.chkSelfPicker);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.txtPlz);
            this.Controls.Add(this.lblPlz);
            this.Controls.Add(this.txtOrt);
            this.Controls.Add(this.lblOrt);
            this.Controls.Add(this.txtStrasse);
            this.Controls.Add(this.lblStrasse);
            this.Controls.Add(this.txtNachName);
            this.Controls.Add(this.lblNachname);
            this.Controls.Add(this.txtVorname);
            this.Controls.Add(this.lblVorname);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.lblTelefon);
            this.Controls.Add(this.btnInsert);
            this.Name = "FormCustomerDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KundenDetails";
            this.Load += new System.EventHandler(this.FormCustomerDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Label lblTelefon;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Label lblVorname;
        private System.Windows.Forms.TextBox txtVorname;
        private System.Windows.Forms.Label lblNachname;
        private System.Windows.Forms.TextBox txtNachName;
        private System.Windows.Forms.Label lblStrasse;
        private System.Windows.Forms.TextBox txtStrasse;
        private System.Windows.Forms.Label lblOrt;
        private System.Windows.Forms.TextBox txtOrt;
        private System.Windows.Forms.Label lblPlz;
        private System.Windows.Forms.TextBox txtPlz;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.CheckBox chkSelfPicker;
        private System.Windows.Forms.Button btnCancel;
    }
}

