﻿namespace PizzaService
{
    partial class FormCustomerOrderPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        // To detect redundant calls
        private bool _disposed = false;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (_streams != null)
            {
                foreach (var stream in _streams)
                    stream.Close();
                _streams = null;
            }
            reportViewerPrint.LocalReport.ReleaseSandboxAppDomain();
            _disposed = true;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.CustomerBillsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            // TODO Microsoft.Reporting.WinForms.ReportViewer no longer supported.
            this.reportViewerPrint = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerBillsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // CustomerBillsBindingSource
            // 
            this.CustomerBillsBindingSource.DataSource = typeof(PizzaServiceDB.DbBase.CustomerBills);
            // 
            // reportViewerPrint
            // 
            this.reportViewerPrint.AutoSize = true;
            this.reportViewerPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource.Name = "DataSet1";
            reportDataSource.Value = this.CustomerBillsBindingSource;
            this.reportViewerPrint.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewerPrint.LocalReport.ReportEmbeddedResource = "PizzaService.ReportOrderPrint.rdlc";
            this.reportViewerPrint.Location = new System.Drawing.Point(0, 0);
            this.reportViewerPrint.Name = "reportViewerPrint";
            this.reportViewerPrint.ServerReport.BearerToken = null;
            this.reportViewerPrint.Size = new System.Drawing.Size(581, 404);
            this.reportViewerPrint.TabIndex = 0;
            this.reportViewerPrint.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.FullPage;
            // 
            // FormCustomerOrderPrint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(581, 404);
            this.Controls.Add(this.reportViewerPrint);
            this.Name = "FormCustomerOrderPrint";
            this.Text = "Bestellungdruck";
            ((System.ComponentModel.ISupportInitialize)(this.CustomerBillsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        // TODO Microsoft.Reporting.WinForms.ReportViewer no longer supported.
        private Microsoft.Reporting.WinForms.ReportViewer reportViewerPrint;
        private System.Windows.Forms.BindingSource CustomerBillsBindingSource;
    }
}