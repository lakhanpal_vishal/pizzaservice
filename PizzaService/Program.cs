﻿using log4net;
using log4net.Util;
using PizzaService.BusinessLogic;
using PizzaService.PizzaServiceLocator;
using PizzaService.PizzaServiceUnitOfWork;
using PizzaServiceDB.DbBase;
using PizzaServiceDB.PizzaServiceRepository;
using PizzaServiceDB.PizzaServiceUnitOfWork;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Windows.Forms;
namespace PizzaService
{
    internal static class Program
    {
        private static Container _container;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>//
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            log4net.Config.XmlConfigurator.Configure();

            LogLog.LogReceived += (_, e) =>
            {
                if (e.LogLog.Prefix.Contains("ERROR"))
                {
                    throw new ConfigurationErrorsException(e.LogLog.Message,
                        e.LogLog.Exception);
                }
            };

            Bootstrap();

            var validation = _container.GetInstance<IValidation>();
            var serCustomer = _container.GetInstance<IPizService<Customer>>();
            var serItems = _container.GetInstance<IPizService<Items>>();
            var customer = _container.GetInstance<Customer>();
            var itPriceCalculation = _container.GetInstance<IItemPriceCalculation>();
            var unOfWork = _container.GetInstance<IPizService<ResultNextFreeRowId>>();
            var serCustomerBills = _container.GetInstance<IPizService<CustomerBills>>();
            var serLogger = _container.GetInstance<IPizzaServiceLogger>();
            //@Todo too much injection needs to be changed with Facade pattern
            Application.Run(new FormCustomerDetails(validation, itPriceCalculation, serItems,
                                                    serCustomer, customer, unOfWork,
                                                    serCustomerBills, serLogger)
                            );
        }

        private static void Bootstrap()
        {
            _container = new Container();
            _container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            _container.Register<IItemPriceCalculation, ItemPriceCalculation>(Lifestyle.Singleton);
            _container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            _container.Register<DbContext, PizzaServiceContext>(Lifestyle.Scoped);
            _container.Register(typeof(IPizzaServiceRepository<>), typeof(PizzaServiceRepository<>), Lifestyle.Scoped);
            _container.Register<IValidation, Validation>(Lifestyle.Singleton);
            _container.RegisterSingleton<IPizzaServiceLogger>(() => new Log4NetAdapter(LogManager.GetLogger(typeof(object))));
            _container.Register<IConfiguration, Configuration>(Lifestyle.Singleton);
            _container.Register<Customer>(Lifestyle.Singleton);

            var entityTypes = _container.GetTypesToRegister(typeof(IEntity), typeof(IEntity).Assembly);
            var entityTypesrRegistrations = (
                from type in entityTypes
                select Lifestyle.Singleton.CreateRegistration(type, _container)
                ).ToArray(); // This call is needed to prevent double enumeration!!

            _container.Collection.Register<IEntity>(entityTypesrRegistrations);
            _container.Register(typeof(IPizService<>), typeof(PizService<>), Lifestyle.Scoped);
            _container.RegisterDecorator(typeof(IPizService<>), typeof(ScopeRequestDecorator<>), Lifestyle.Singleton);

            _container.Verify();
        }

        //public static void RegisterDisposableTransient<TService, TImplementation>(
        //this Container c)
        //where TImplementation : class, IDisposable, TService
        //where TService : class
        //    {
        //        var scoped = Lifestyle.Scoped;
        //        var r = Lifestyle.Transient.CreateRegistration<TService, TImplementation>(c);
        //        r.SuppressDiagnosticWarning(DiagnosticType.DisposableTransientComponent, "ignore");
        //        c.AddRegistration(typeof(TService), r);
        //        c.RegisterInitializer<TImplementation>(o => scoped.RegisterForDisposal(c, o));
        //    }
    }
}
