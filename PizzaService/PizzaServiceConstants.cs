﻿
namespace PizzaService
{
    /// <summary>
    /// This will be responsible to avoid magic numbers/strings
    /// polluting the source code.
    /// </summary>
    public static class PizzaServiceConstants
    {
        public const int HIDE_LOC_X = 250;
        public const int HIDE_LOC_Y = 180;
        public const int HIDE_LOC_DELTA_Y = 50;
        public const int SHOW_LOC_X = 77;
        public const int SHOW_LOC_DELTA_X = 248;
        public const int SHOW_LOC_Y = 109;
        public const int BTN_LOC_X = 232;
        public const int BTN_LOC_Y = 411;
        public const int CHK_BTN_LOC_X = 250;
        public const int CHK_BTN_LOC_Y = 280;
        public const string ABHOLER = "Abholer";
        public const string ANZAHL = "Anzahl";
        public const string PREIS = "Preis";
        public const string PRICE = "Price";
        public const string GRÖSSE = "Größe";
        public const string NUMMER = "Nummer";
        public const string NAME = "Name";
        public const string PLUS = "+";
        public const string ASTERIX = "*";
        public const string SINGLE = "Single";
        public const string JUMBO = "Jumbo";
        public const string FAMILY = "Family";
        public const string PARTY = "Party";
        public const string COMMA = ",";
        public const string COMMAWITHSPACE = ", ";
        public const string CURRENCY = "c";
        public const string MIT = " mit ";
        public const string UND = " und ";
    }
}
