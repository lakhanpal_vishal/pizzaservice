﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using PC = PizzaService.PizzaServiceConstants;

namespace PizzaService
{
    internal static class Utilites
    {
        /// <summary>
        /// Self Explanatory
        /// </summary>
        /// <returns></returns>
        public static string ConvertDecimalToString(decimal dInputVal)
        {
            return dInputVal.ToString();//string.Format(CultureInfo.CurrentCulture, "{0:0.00}", dInputVal);
        }

        /// <summary>
        /// Self Explanatory
        /// </summary>
        /// <param name="strInputVal"></param>
        /// <returns></returns>
        public static decimal ConvertStringToDecimal(string strInputVal)
        {
            decimal.TryParse(strInputVal, NumberStyles.Number | NumberStyles.AllowCurrencySymbol, CultureInfo.CurrentCulture, out var dRetValue);
            return dRetValue;
        }

        /// <summary>
        /// Here we prepare the itemname depending upon its content.
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="trimmedArrItemsFirstElement"></param>
        /// <param name="trimmedModArrItemsWithoutFirstElement"></param>
        /// <param name="trimmedOrigArrItemsWithoutFirstElement"></param>
        public static void AdjustItemName(string itemName,
                                            out string trimmedArrItemsFirstElement,
                                            out IEnumerable<string> trimmedModArrItemsWithoutFirstElement,
                                            out IEnumerable<string> trimmedOrigArrItemsWithoutFirstElement)
        {
            GetFirstandLastSeprator(itemName, out var firstSep, out var lastSep);

            string[] trimmedArrItems = itemName.
                                        Split(GetSeprators(), StringSplitOptions.RemoveEmptyEntries)
                                       .Select(i => i.Trim()).ToArray();

            trimmedOrigArrItemsWithoutFirstElement = trimmedArrItems.Skip(1);
            trimmedModArrItemsWithoutFirstElement = trimmedOrigArrItemsWithoutFirstElement;
            if (string.IsNullOrEmpty(firstSep))
            {
                trimmedArrItemsFirstElement = itemName;
                trimmedModArrItemsWithoutFirstElement = new List<string>();
                return;
            }

            if (trimmedModArrItemsWithoutFirstElement.Any())
            {
                var list = trimmedModArrItemsWithoutFirstElement.ToList();
                list[list.Count - 1] = lastSep + trimmedModArrItemsWithoutFirstElement.Last();
                trimmedModArrItemsWithoutFirstElement = list;
            }
            trimmedArrItemsFirstElement = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(trimmedArrItems[0]) + firstSep;
        }

        /// <summary>
        /// self explanatory
        /// </summary>
        /// <returns></returns>
        public static string[] GetSeprators()
        {
            return new[] { PC.MIT, PC.COMMA, PC.UND };
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="itemName"></param>
        /// <param name="firstSep"></param>
        /// <param name="lastSep"></param>
        public static void GetFirstandLastSeprator(string itemName, out string firstSep, out string lastSep)
        {
            var dictSeprartors = new Dictionary<int, string>();
            firstSep = PC.COMMA;
            lastSep = string.Empty;
            foreach (string sep in GetSeprators())
            {
                var index = itemName.IndexOf(sep.Trim(), StringComparison.Ordinal);
                if (index != -1)
                {
                    dictSeprartors[index] = sep;
                }
            }
            if (dictSeprartors.Count > 0)
            {
                var orderedDict = dictSeprartors.OrderBy(k => k.Key);
                firstSep = orderedDict.First().Value.StartsWith(PC.COMMA, StringComparison.Ordinal) ? PC.COMMAWITHSPACE : orderedDict.First().Value;
                lastSep = orderedDict.Last().Value.StartsWith(PC.COMMA, StringComparison.Ordinal) ? PC.COMMAWITHSPACE : orderedDict.Last().Value;
            }
        }
    }
}
