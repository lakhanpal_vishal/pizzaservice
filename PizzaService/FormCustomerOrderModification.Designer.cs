﻿namespace PizzaService
{
    partial class FormCustomerOrderModification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lstBoxOptionPaidSubItems = new System.Windows.Forms.ListBox();
            this.lstBoxOptionFreeSubItems = new System.Windows.Forms.ListBox();
            this.lblBezahlt = new System.Windows.Forms.Label();
            this.lblUnbezahlt = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddSubItem = new System.Windows.Forms.Button();
            this.btnRemoveSubItem = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtExtraSubItems = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.checkBoxFreeSubItems = new System.Windows.Forms.CheckBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnExtraSubItems = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lstBoxCurrentSubItems = new System.Windows.Forms.ListBox();
            this.lblAddedSubItems = new System.Windows.Forms.Label();
            this.lblModifyingItem = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.PowderBlue;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lstBoxOptionPaidSubItems, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lstBoxOptionFreeSubItems, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblBezahlt, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblUnbezahlt, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(456, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(385, 403);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // lstBoxOptionPaidSubItems
            // 
            this.lstBoxOptionPaidSubItems.BackColor = System.Drawing.Color.Wheat;
            this.lstBoxOptionPaidSubItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstBoxOptionPaidSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstBoxOptionPaidSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxOptionPaidSubItems.ForeColor = System.Drawing.Color.Tomato;
            this.lstBoxOptionPaidSubItems.FormattingEnabled = true;
            this.lstBoxOptionPaidSubItems.ItemHeight = 20;
            this.lstBoxOptionPaidSubItems.Location = new System.Drawing.Point(3, 33);
            this.lstBoxOptionPaidSubItems.Name = "lstBoxOptionPaidSubItems";
            this.lstBoxOptionPaidSubItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstBoxOptionPaidSubItems.Size = new System.Drawing.Size(186, 367);
            this.lstBoxOptionPaidSubItems.TabIndex = 1;
            this.lstBoxOptionPaidSubItems.SelectedIndexChanged += new System.EventHandler(this.LstBoxOptionPaidSubItems_SelectedIndexChanged);
            // 
            // lstBoxOptionFreeSubItems
            // 
            this.lstBoxOptionFreeSubItems.BackColor = System.Drawing.Color.PowderBlue;
            this.lstBoxOptionFreeSubItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstBoxOptionFreeSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstBoxOptionFreeSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxOptionFreeSubItems.ForeColor = System.Drawing.Color.Red;
            this.lstBoxOptionFreeSubItems.FormattingEnabled = true;
            this.lstBoxOptionFreeSubItems.ItemHeight = 16;
            this.lstBoxOptionFreeSubItems.Location = new System.Drawing.Point(195, 33);
            this.lstBoxOptionFreeSubItems.Name = "lstBoxOptionFreeSubItems";
            this.lstBoxOptionFreeSubItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstBoxOptionFreeSubItems.Size = new System.Drawing.Size(187, 367);
            this.lstBoxOptionFreeSubItems.TabIndex = 3;
            this.lstBoxOptionFreeSubItems.SelectedIndexChanged += new System.EventHandler(this.LstBoxOptionFreeSubItems_SelectedIndexChanged);
            // 
            // lblBezahlt
            // 
            this.lblBezahlt.AutoSize = true;
            this.lblBezahlt.BackColor = System.Drawing.Color.PowderBlue;
            this.lblBezahlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblBezahlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblBezahlt.ForeColor = System.Drawing.Color.Crimson;
            this.lblBezahlt.Location = new System.Drawing.Point(3, 0);
            this.lblBezahlt.Name = "lblBezahlt";
            this.lblBezahlt.Size = new System.Drawing.Size(186, 30);
            this.lblBezahlt.TabIndex = 4;
            this.lblBezahlt.Text = "Bezahlt";
            this.lblBezahlt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnbezahlt
            // 
            this.lblUnbezahlt.AutoSize = true;
            this.lblUnbezahlt.BackColor = System.Drawing.Color.PowderBlue;
            this.lblUnbezahlt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnbezahlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblUnbezahlt.ForeColor = System.Drawing.Color.Crimson;
            this.lblUnbezahlt.Location = new System.Drawing.Point(195, 0);
            this.lblUnbezahlt.Name = "lblUnbezahlt";
            this.lblUnbezahlt.Size = new System.Drawing.Size(187, 30);
            this.lblUnbezahlt.TabIndex = 5;
            this.lblUnbezahlt.Text = "Unbezahlt";
            this.lblUnbezahlt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.PowderBlue;
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.btnAddSubItem, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnRemoveSubItem, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(334, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.69191F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.30809F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(122, 403);
            this.tableLayoutPanel3.TabIndex = 15;
            // 
            // btnAddSubItem
            // 
            this.btnAddSubItem.BackColor = System.Drawing.Color.LightBlue;
            this.btnAddSubItem.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAddSubItem.Enabled = false;
            this.btnAddSubItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubItem.ForeColor = System.Drawing.Color.Crimson;
            this.btnAddSubItem.Location = new System.Drawing.Point(3, 152);
            this.btnAddSubItem.Name = "btnAddSubItem";
            this.btnAddSubItem.Size = new System.Drawing.Size(116, 29);
            this.btnAddSubItem.TabIndex = 2;
            this.btnAddSubItem.Text = "Hinzufügen";
            this.btnAddSubItem.UseVisualStyleBackColor = false;
            this.btnAddSubItem.Click += new System.EventHandler(this.BtnAddSubItem_ClickAsync);
            // 
            // btnRemoveSubItem
            // 
            this.btnRemoveSubItem.BackColor = System.Drawing.Color.LightBlue;
            this.btnRemoveSubItem.Enabled = false;
            this.btnRemoveSubItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemoveSubItem.ForeColor = System.Drawing.Color.Crimson;
            this.btnRemoveSubItem.Location = new System.Drawing.Point(3, 187);
            this.btnRemoveSubItem.Name = "btnRemoveSubItem";
            this.btnRemoveSubItem.Size = new System.Drawing.Size(116, 33);
            this.btnRemoveSubItem.TabIndex = 3;
            this.btnRemoveSubItem.Text = "Entfernen";
            this.btnRemoveSubItem.UseVisualStyleBackColor = false;
            this.btnRemoveSubItem.Click += new System.EventHandler(this.BtnRemoveSubItem_ClickAsync);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.PowderBlue;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.78049F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.21951F));
            this.tableLayoutPanel2.Controls.Add(this.txtExtraSubItems, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblPrice, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.checkBoxFreeSubItems, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtPrice, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnExtraSubItems, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 297);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(334, 106);
            this.tableLayoutPanel2.TabIndex = 16;
            // 
            // txtExtraSubItems
            // 
            this.txtExtraSubItems.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtExtraSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtExtraSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExtraSubItems.ForeColor = System.Drawing.Color.Maroon;
            this.txtExtraSubItems.Location = new System.Drawing.Point(165, 36);
            this.txtExtraSubItems.Name = "txtExtraSubItems";
            this.txtExtraSubItems.Size = new System.Drawing.Size(166, 26);
            this.txtExtraSubItems.TabIndex = 9;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.Crimson;
            this.lblPrice.Location = new System.Drawing.Point(3, 0);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(156, 33);
            this.lblPrice.TabIndex = 4;
            this.lblPrice.Text = "Summe";
            this.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // checkBoxFreeSubItems
            // 
            this.checkBoxFreeSubItems.AutoSize = true;
            this.checkBoxFreeSubItems.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxFreeSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBoxFreeSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFreeSubItems.ForeColor = System.Drawing.Color.Crimson;
            this.checkBoxFreeSubItems.Location = new System.Drawing.Point(3, 36);
            this.checkBoxFreeSubItems.Name = "checkBoxFreeSubItems";
            this.checkBoxFreeSubItems.Size = new System.Drawing.Size(156, 27);
            this.checkBoxFreeSubItems.TabIndex = 12;
            this.checkBoxFreeSubItems.Text = "SubItems  ";
            this.checkBoxFreeSubItems.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxFreeSubItems.UseVisualStyleBackColor = true;
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.ForeColor = System.Drawing.Color.Maroon;
            this.txtPrice.Location = new System.Drawing.Point(165, 3);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(166, 26);
            this.txtPrice.TabIndex = 5;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.01961F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.98039F));
            this.tableLayoutPanel5.Controls.Add(this.btnCancel, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.btnOK, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 69);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(156, 34);
            this.tableLayoutPanel5.TabIndex = 13;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.PowderBlue;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.Crimson;
            this.btnCancel.Location = new System.Drawing.Point(79, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(74, 28);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.PowderBlue;
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOK.ForeColor = System.Drawing.Color.Crimson;
            this.btnOK.Location = new System.Drawing.Point(3, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(70, 28);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // btnExtraSubItems
            // 
            this.btnExtraSubItems.BackColor = System.Drawing.Color.PowderBlue;
            this.btnExtraSubItems.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnExtraSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExtraSubItems.ForeColor = System.Drawing.Color.Crimson;
            this.btnExtraSubItems.Location = new System.Drawing.Point(165, 69);
            this.btnExtraSubItems.Name = "btnExtraSubItems";
            this.btnExtraSubItems.Size = new System.Drawing.Size(166, 31);
            this.btnExtraSubItems.TabIndex = 10;
            this.btnExtraSubItems.Text = "Update SubItems";
            this.btnExtraSubItems.UseVisualStyleBackColor = false;
            this.btnExtraSubItems.Click += new System.EventHandler(this.BtnExtraSubItems_ClickAsync);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.PowderBlue;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.lstBoxCurrentSubItems, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblAddedSubItems, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblModifyingItem, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.24164F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.75836F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(334, 297);
            this.tableLayoutPanel4.TabIndex = 17;
            // 
            // lstBoxCurrentSubItems
            // 
            this.lstBoxCurrentSubItems.BackColor = System.Drawing.Color.Azure;
            this.lstBoxCurrentSubItems.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstBoxCurrentSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstBoxCurrentSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxCurrentSubItems.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lstBoxCurrentSubItems.FormattingEnabled = true;
            this.lstBoxCurrentSubItems.ItemHeight = 20;
            this.lstBoxCurrentSubItems.Location = new System.Drawing.Point(3, 44);
            this.lstBoxCurrentSubItems.Name = "lstBoxCurrentSubItems";
            this.lstBoxCurrentSubItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstBoxCurrentSubItems.Size = new System.Drawing.Size(328, 224);
            this.lstBoxCurrentSubItems.TabIndex = 0;
            this.lstBoxCurrentSubItems.SelectedIndexChanged += new System.EventHandler(this.LstBoxCurrentSubItems_SelectedIndexChanged);
            // 
            // lblAddedSubItems
            // 
            this.lblAddedSubItems.AutoSize = true;
            this.lblAddedSubItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAddedSubItems.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddedSubItems.ForeColor = System.Drawing.Color.Crimson;
            this.lblAddedSubItems.Location = new System.Drawing.Point(3, 271);
            this.lblAddedSubItems.Name = "lblAddedSubItems";
            this.lblAddedSubItems.Size = new System.Drawing.Size(328, 26);
            this.lblAddedSubItems.TabIndex = 7;
            this.lblAddedSubItems.Text = "Neue-SubItems:";
            // 
            // lblModifyingItem
            // 
            this.lblModifyingItem.AutoEllipsis = true;
            this.lblModifyingItem.BackColor = System.Drawing.Color.PowderBlue;
            this.lblModifyingItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblModifyingItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModifyingItem.ForeColor = System.Drawing.Color.Crimson;
            this.lblModifyingItem.Location = new System.Drawing.Point(3, 0);
            this.lblModifyingItem.Name = "lblModifyingItem";
            this.lblModifyingItem.Size = new System.Drawing.Size(328, 41);
            this.lblModifyingItem.TabIndex = 1;
            this.lblModifyingItem.Text = "labelItem";
            // 
            // FormCustomerOrderModification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 403);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FormCustomerOrderModification";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bestellunganpassung";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox lstBoxOptionPaidSubItems;
        private System.Windows.Forms.ListBox lstBoxOptionFreeSubItems;
        private System.Windows.Forms.Label lblBezahlt;
        private System.Windows.Forms.Label lblUnbezahlt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnAddSubItem;
        private System.Windows.Forms.Button btnRemoveSubItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox txtExtraSubItems;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.CheckBox checkBoxFreeSubItems;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnExtraSubItems;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ListBox lstBoxCurrentSubItems;
        private System.Windows.Forms.Label lblAddedSubItems;
        private System.Windows.Forms.Label lblModifyingItem;
    }
}