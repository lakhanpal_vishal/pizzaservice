﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace PizzaServiceDB.PizzaServiceUnitOfWork
{
    public interface IUnitOfWork
    {
        Task Commit();
        IEnumerable<T> ExecuteRawSqlQuery<T>(T entity, string sql) where T : class;
    }
}
