﻿using PizzaServiceDB.PizzaServiceUnitOfWork;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace PizzaService.PizzaServiceUnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;

        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Commit()
        {
            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public IEnumerable<T> ExecuteRawSqlQuery<T>(T entity, string sql) where T : class
        {
            return _dbContext.Database.SqlQuery<T>(sql).ToList();
        }
    }
}
