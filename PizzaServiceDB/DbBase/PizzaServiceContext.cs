﻿using System.Data.Entity;
using System.Data.Entity.Core.Common;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Data.SQLite.EF6;
namespace PizzaServiceDB.DbBase
{
    public class SQLiteConfiguration : DbConfiguration
    {
        public SQLiteConfiguration()
        {
            SetProviderFactory("System.Data.SQLite", SQLiteFactory.Instance);
            SetProviderFactory("System.Data.SQLite.EF6", SQLiteProviderFactory.Instance);
            SetProviderServices("System.Data.SQLite", (DbProviderServices)SQLiteProviderFactory.Instance.GetService(typeof(DbProviderServices)));
        }
    }

    /// <summary>
    /// This class will be responsible for the communication between the DB
    /// and the ORM mappers in the source code.
    /// </summary>
    public sealed class PizzaServiceContext : DbContext
    {
        #region PizzaServiceContext Members
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Items> CustomerItem { get; set; }
        public DbSet<SubItems> SubItem { get; set; }
        public DbSet<CustomerBills> CustomerBill { get; set; }
        #endregion PizzaServiceContext Members

        public PizzaServiceContext() : base("name=PizzaServiceContext")
        {
        }
      
        #region PizzaServiceContext Methods
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<PizzaServiceContext>(null);
            //Many to many between Item and SubItems Table
            modelBuilder.Entity<Items>().HasMany(g => g.listSubItems)
                .WithMany(p => p.listItems).Map(c =>
            {
                c.MapLeftKey("ItemId");
                c.MapRightKey("SubItemId");
                c.ToTable("Items_SubItems");
            });
            // Do not not pluralize table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
        #endregion PizzaServiceContext Methods
    }
}
