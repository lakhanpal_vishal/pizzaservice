﻿namespace PizzaServiceDB.DbBase
{
    //This class holds the info about the Customer
   public sealed class Customer : IEntity
    {
        #region IEntity Members
        public int Id { get; set; }
        #endregion
        #region Customer Members
        public string City { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? Postalcode { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        #endregion Customer Members
    }
}
