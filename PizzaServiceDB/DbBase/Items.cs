﻿
using System.Collections.Generic;

namespace PizzaServiceDB.DbBase
{
    /// <summary>
    /// This will be responsible for the current customer's
    /// ordered Item like Pizza, Salat etc.
    /// For more info  see the 'Items' table in the DB
    /// </summary>
    public class Items : IEntity
    {
        #region IEntity Members
        public int Id { get; set; }
        #endregion
        #region Items Members
        public string ItemName { get; set; }
        public int ItemId { get; set; }
        public decimal PriceSingle { get; set; }
        public decimal PriceJumbo { get; set; }
        public decimal PriceFamily { get; set; }
        public decimal PriceParty { get; set; }
        public virtual List<SubItems> listSubItems { get; set; }
    }
    #endregion Items Methods
}
