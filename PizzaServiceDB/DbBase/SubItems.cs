﻿
using System.Collections.Generic;

namespace PizzaServiceDB.DbBase
{
    /// <summary>
    /// This class will be responsible for adding/removing subitems(subparts)
    /// to the Item of the customer's order. 
    /// For more info  see the 'subItems' table in the DB
    /// </summary>
    public class SubItems : IEntity
    {
        #region IEntity Members
        public int Id { get; set; }
        #endregion
        #region SubItems Members
        public string SubItemText { get; private set; }
        public string SubItemCategory { get; private set; }
        public string SubItemFreeText { get; private set; }
        public virtual List<Items> listItems { get; set; }
        #endregion SubItems Members
    }
}
