﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PizzaServiceDB.DbBase
{
    /// <summary>
    /// Here we will retrieve the next free rowid from the CustomerBills Table
    /// </summary>
    public sealed class ResultNextFreeRowId
    {
        private int seq;
        public int Seq
        {
            get => seq;
            set
            {
                if (value < 0)
                    throw new InvalidOperationException($"Value of the: {nameof(seq)} cannot be less than zero");
                seq = value;
            }
        }
    }

    /// <summary>
    /// The class will be responsible for the Current Bill for the Customer
    /// </summary>
    public class CustomerBills : IEntity
    {
        #region IEntity Members
        public int Id { get; set; }
        #endregion
        #region CustomerBills Members
        public int CustomerBillNumber { get; set; }
        public int CustomerBillItemId { get; set; }
        public int CustomerBillNumberOfItems { get; set; }
        public string CustomerBillItemName { get; set; }
        public string CustomerBillItemSize { get; set; }
        public decimal CustomerBillItemPrice { get; set; }
        public DateTime CustomerBillPrintTime { get; set; }
        public int CustomerRefId { get; set; }
        [ForeignKey("CustomerRefId")]
        public virtual Customer CurrentCustomer { get; set; }
        #endregion CustomerBills Members
    }
}
