﻿namespace PizzaServiceDB.DbBase
{
    /// <summary>
    /// interface for the primary key of all the DB tables
    /// </summary>
    public interface IEntity
    {
        int Id { get; }
    }
}
