﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PizzaServiceDB.PizzaServiceRepository
{
    /// <summary>
    /// This class will be responsible for the DB operations.
    /// </summary>
    public class PizzaServiceRepository<T> : IPizzaServiceRepository<T> where T : class
    {
        protected DbSet<T> DbSet;
        protected DbContext DataContext;

        public PizzaServiceRepository(DbContext dataContext)
        {
            DataContext = dataContext;
            DbSet = dataContext.Set<T>();
        }

        #region IPizzaServiceRepository<T> Methods

        /// <summary>
        /// Responsible for inserting a new entity into the DB
        /// </summary>
        /// <param name="entity"></param>
        public void Insert(T entity)
        {
            DbSet.Add(entity);
            // await SaveChangesAsync();
        }

        /// <summary>
        ///  Responsible for inserting a new range of entities into the DB
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public void InsertRange(List<T> entity)
        {
            DbSet.AddRange(entity);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="navProperty"></param>
        /// <returns></returns>
        public async Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty)
        {
            if (!string.IsNullOrEmpty(navProperty))
            {
                return await Task.Run(() => DbSet.Where(predicate).
                Include(navProperty).ToList()).ConfigureAwait(false);
            }

            return await Task.Run(() => DbSet.Where(predicate).ToList())
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Returns all the Entities from the current DB Table.
        /// </summary>
        /// <returns></returns>
        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        /// <summary>
        /// Returns an Entity based upon the id of the Table.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>T</returns>
        public T GetById(int id)
        {
            return DbSet.Find(id);
        }

        /// <summary>
        /// Updates an Existing Entity.
        /// </summary>
        /// <returns>none</returns>
        public void Update(T entity)
        {
            DbSet.Attach(entity);
            DataContext.Entry(entity).State = EntityState.Modified;
        }


        #endregion IPizzaServiceRepository<T> Methods
    }
}
