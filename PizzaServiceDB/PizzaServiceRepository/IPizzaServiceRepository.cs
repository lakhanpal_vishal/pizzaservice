﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
//using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace PizzaServiceDB.PizzaServiceRepository
{
    public interface IPizzaServiceRepository<T> where T : class
    {
        void InsertRange(List<T> entity);
        void Insert(T entity);
        void Update(T entity);
        Task<IEnumerable<T>> SearchForAsync(Expression<Func<T, bool>> predicate, string navProperty = null);
        IQueryable<T> GetAll();
        T GetById(int id);
    }
}
